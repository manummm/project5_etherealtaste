<?php

class UserManager extends Db {
    
    // Log the user with the username & password sent
    public function saveSessionToken($username, $token_session) {
        
        $sql = 'UPDATE project5_users SET token_session = :token_session WHERE username = :username';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':token_session', $token_session, PDO::PARAM_STR, 255);
        $query->bindParam(':username', $username, PDO::PARAM_STR, 255);
        // Execute the query
        $query->execute();
        return TRUE;
        
    }
    
    // Log out (unset the current session & destroy cookies)
    public function logout() {
        // If connected, deconnection & redirection to the homepage
        if(isset($_SESSION['session'])){
            // ------------------------------------------------------------------------------------------------
            // -------------------------- Delete session variables & the session ------------------------------
            // ------------------------------------------------------------------------------------------------
            // Unset all of the session variables ( Same as ==>session_unset() )
            $_SESSION = array();
            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!
            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
            }
            // Destroy the session.
            session_destroy();
            // ------------------------------------------------------------------------------------------------
            // -------------------------- Delete all the cookies ----------------------------------------------
            // ------------------------------------------------------------------------------------------------
            // http://php.net/manual/fr/function.setcookie.php
            
            if (isset($_COOKIE['token_session'])) {
                unset($_COOKIE['token_session']);
                // empty value and old timestamp
                setcookie('token_session', '', time() - 3600, '/', null, false, false);
            }
            if (isset($_COOKIE['username'])) {
                unset($_COOKIE['username']);
                // empty value and old timestamp
                setcookie('username', '', time() - 3600, '/', null, false, false);
            }
            // ------------------------------------------------------------------------------------------------
            
            return TRUE;
        }
    }
    
    // Get the user from the database with the given username 
    public function getUser($username) {
        
        $sql = 'SELECT id, username, password_hash as passwordHash, email, inscription_date as inscriptionDate, token_session as TokenSession, accreditation_id as idAccreditationLevel'
             . ' FROM project5_users'
             . ' WHERE username = :username';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':username', $username, PDO::PARAM_STR, 255);
        // Execute the query
        $query->execute();
        // Look if there is a result & return it
        if ($query->rowCount() == 1){
            $data = $query->fetch();  // Access to the first result line            
            return new User($data);
        }else{
            return NULL;
        }
        
    }
    
    // -------------------------------------------------------------------------
    // ------------ Not active yet, future features -----------------------------
    // -------------------------------------------------------------------------
    
//    public function signin(User $user) {
//        // Not active yet, future feature        
//    }
//    
//    // Check if the Username is already existing in the database, return false if any result found, otherwise return true.
//    public function validUsername($username) {
//        $sql = 'SELECT * FROM project5_users WHERE username = :username';
//        // Prepare the query
//        $query = $this->getDb()->prepare($sql);
//        // Bind parameters
//        $query->bindParam(':username', $username, PDO::PARAM_STR, 255);
//        // Execute the query
//        $query->execute();
//        if ($req->rowCount() == 0){
//            return TRUE;
//        }
//        else {
//            return FALSE;
//        }
//    }
//    
//    // Delete a user
//    public function delete($id) {
//        // Delete an user
//        $sql = 'DELETE FROM project5_users WHERE id = :id';
//        // Prepare the query
//        $query = $this->getDb()->prepare($sql);
//        // Bind parameters
//        $query->bindParam(':id', $id, PDO::PARAM_INT);
//        // Execute the query
//        $query->execute();
//        return TRUE;
//    }
//    
//    // Email activation
//    public function emailActivation($username) {
//        // (Not active / Future Feature)
//    }    
//    
//    // Give or remove permission(s) to the specified User
//    public function modifyAccreditationLevel($id) {
//        // (Not active / Future Feature)
//    }
//    
//    // Allow to modify an Username
//    public function editUsername($id) {
//        // (Not active / Future Feature)
//    }
    
}