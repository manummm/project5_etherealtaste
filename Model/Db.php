<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of db
 *
 * @author Manu
 */

class Db {
    
    //--------------------------- ATTRIBUTES ---------------------------------//
    //------------------------------------------------------------------------//

    // PDO object of access to the database
    private $_db;
    // PDO object connection's attributes
    private $_options;
    // PDO object configuration
    private $_driver, $_host, $_database, $_charset, $_login, $_password;

    //-------------------------- CONSTRUCTOR ---------------------------------//
    //------------------------------------------------------------------------//
    
    public function __construct() {
        // Set the connection's options
        $this->setOptions();
        $this->setConfig();
    }

    //---------------------------- GETTERS -----------------------------------//
    //------------------------------------------------------------------------//
    
    // Get the connection's atributes for the PDO object
    private function getOptions() {
        return $this->_options;
    }
    
    // Get the driver for the PDO object configuration
    private function getDriver() {
        return $this->_driver;
    }
    
    // Get the host for the PDO object configuration
    private function getHost() {
        return $this->_host;
    }
    
    // Get the database for the PDO object configuration
    private function getDatabase() {
        return $this->_database;
    }
    
    // Get the charset for the PDO object configuration
    private function getCharset() {
        return $this->_charset;
    }
    
    // Get the login for the PDO object configuration
    private function getLogin() {
        return $this->_login;
    }
    
    // Get the password for the PDO object configuration
    private function getPassword() {
        return $this->_password;
    }
    
    //---------------------------- SETTERS -----------------------------------//
    //------------------------------------------------------------------------//
    
    // Set the connection's atributes for the PDO object
    private function setOptions() {
        
        // ---------------------------------------------------------------------
        // Connection settings -------------------------------------------------
        // ---------------------------------------------------------------------
        
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => FALSE
        ];
                
        $this->_options = $options;
        
    }
    
    // Set the config
    private function setConfig() {
        
        // ---------------------------------------------------------------------
        // Configuration settings ----------------------------------------------
        // ---------------------------------------------------------------------
        $config = array(
            "driver"    => "mysql",
            "host"      => "localhost",
            "database"  => "project5_etherealtaste",
            "charset"   => "utf8",
            "login"      => "root",
            "password"  => ""
        );
        
        $this->_driver   = $config["driver"];
        $this->_host     = $config["host"];
        $this->_database = $config["database"];
        $this->_charset  = $config["charset"];
        $this->_login     = $config["login"];
        $this->_password = $config["password"];
        
    }
    
    //--------------------------- FUNCTIONS ----------------------------------//
    //------------------------------------------------------------------------//
        
    // Returns a PDO object of access to the DB (by initiating the connection if needed)
    protected function getDb() {
        if ($this->_db == null) {
            // Création de la connexion
            $this->_db = new PDO($this->getDriver().':host='.$this->getHost().';dbname='.$this->getDatabase().';charset='.$this->getCharset(), $this->getLogin(), $this->getPassword(), $this->getOptions());
        }
        return $this->_db;
    }
    
    // Return the id of the last line inserted in the database
    protected function lastId() {
        return $this->getDb()->lastInsertId();
    }
}