<?php

/**
 * Description of DateUtilitary
 *
 * @author Manu
 */

class DateUtilitary extends DateTime {
    
    // If the count is > 1, it puts the following text in the plural
    private function pluralize($count, $text) {
        return $count . (($count == 1) ? (" $text") : (" ${text}s"));
    }
    
    public function getOptimizedAge($date) {
        
        // Instance two DateTime objects for the given date & the actual time
        $now = new DateTime();
        $then = new DateTime($date);
        
        // Create an objet DateInterval from these by the  method DateTime::diff() 
        $dateInterval = $now->diff($then);
        
        // Get the right suffix in function of the sign of the interval
        $suffix = ($dateInterval->invert ? ' ago' : ' in the future');
        
        // Rewrite the date in fonction of the time difference
        switch (true) {
            
            // If the time difference is bigger or equal to a year
            case ($dateInterval->y >= 1):
                $OptimizedAge = $this->pluralize($dateInterval->y, 'year') . $suffix;
                break;
            
            // If the time difference is bigger or equal to a month
            case ($dateInterval->m >= 1):
                $OptimizedAge = $this->pluralize($dateInterval->m, 'month') . $suffix;
                break;
            
            // If the time difference is bigger or equal to a day
            case ($dateInterval->d >= 1):
                $OptimizedAge = $this->pluralize($dateInterval->d, 'day') . $suffix;
                break;
            
            // If the time difference is bigger or equal to a hour
            case ($dateInterval->h >= 1):
                $OptimizedAge = $this->pluralize($dateInterval->h, 'hour') . $suffix;
                break;
            
            // If the time difference is bigger or equal to a minute
            case ($dateInterval->i >= 1):
                $OptimizedAge = $this->pluralize($dateInterval->i, 'minute') . $suffix;
                break;
            
            // If the time difference is bigger or equal to a second
            case ($dateInterval->s >= 1):
                $OptimizedAge = $this->pluralize($dateInterval->s, 'second') . $suffix;
                break;
            
            default:
                $OptimizedAge = 'a moment ago';
                break;
            
        }
        
        return $OptimizedAge;
        
    }

}
