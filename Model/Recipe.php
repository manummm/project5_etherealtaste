<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of recipe
 *
 * @author Manu
 */
class Recipe {
    
    //--------------------------- ATTRIBUTES ---------------------------------//
    //------------------------------------------------------------------------//
    
    private $_id;                   // Id of the recipe
    private $_name;                 // Title of the recipe
    private $_author;               // Author of the recipe
    private $_ingredients;          // Ingredients needed for the recipe
    private $_preparation;          // Instructions for making the recipe
    private $_diet;                 // Type of diet (vegetarian, vegan, gluten free, etc...)
    private $_difficulty_level;     // Difficulty to make the recipe (function of the time, skills & equipemet needed)
    private $_difficulty_level_numeric; // Difficulty but in (equivalent) numeric value
    private $_type_of_meal;         // Type of meal (Starter, main course, dessert, snack, etc...)
    private $_nb_views;             // Number of times that the recipe has been consulted
    private $_nb_likes;             // Number of people who likes this recipe
    private $_preparation_time;     // Time required to do the recipe (in minutes)
    private $_publication_date;     // Date of when the recipe is published for the first time
    private $_update_date;          // Date of the last modification made to the recipe (update)
    
    //----------------------------- CONSTRUCTOR ------------------------------//
    //------------------------------------------------------------------------//
    
    public function __construct(array $data) {
        $this->hydrate($data);
    }
    
    //----------------------------- HYDRATATION ------------------------------//
    //------------------------------------------------------------------------//
    
    public function hydrate($data) {
        foreach ($data as $key => $value){
            // Define the name of the corresponding method
            $method = 'set' . ucfirst($key);
            // Check if a such method exist
            // method_exists(object, 'method name')
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }
    
    //----------------------------- GETTERS ----------------------------------//
    //------------------------------------------------------------------------//
    
    public function getId() {
        return $this->_id;
    }
    
    public function getName() {
        return $this->_name;
    }
        
    public function getAuthor() {
        return $this->_author;
    }
        
    public function getIngredients() {
        return $this->_ingredients;
    }
        
    public function getPreparation() {
        return $this->_preparation;
    }
        
    public function getDiet() {
        return $this->_diet;
    }
        
    public function getDifficulty_level() {
        return $this->_difficulty_level;
    }
        
    public function getDifficulty_level_numeric() {
        return $this->_difficulty_level_numeric;
    }
        
    public function getType_of_meal() {
        return $this->_type_of_meal;
    }
        
    public function getNb_views() {
        return $this->_nb_views;
    }
        
    public function getNb_likes() {
        return $this->_nb_likes;
    }
        
    public function getPreparation_time() {
        return $this->_preparation_time;
    }
        
    public function getPublication_date() {
        return $this->_publication_date;
    }
        
    public function getUpdate_date() {
        return $this->_update_date;
    }
    
    public function getOptimizedPublicationDate() {
        $dateOptimizer = new DateUtilitary();
        return $dateOptimizer->getOptimizedAge($this->_publication_date);
    }
    
    public function getOptimizedUpdateDate() {
        $dateOptimizer = new DateUtilitary();
        return $dateOptimizer->getOptimizedAge($this->_update_date);
    }
    
    //----------------------------- SETTERS ----------------------------------//
    //------------------------------------------------------------------------//
    
    public function setId($Id) {
        $id = (int) $Id;
        if($id > 0){
            $this->_id = $id;
        }
    }
    
    public function setName($name){
        if(is_string($name)&& (strlen(utf8_decode($name))) <= 255){
            $this->_name = $name;
        }
    }
    
    public function setAuthor($author){
        if(is_string($author)&& (strlen(utf8_decode($author))) <= 255){
            $this->_author = $author;
        }
    }
    
    public function setIngredients($ingredients){
        if(is_string($ingredients)&& (strlen(utf8_decode($ingredients))) <= 65535){
            $this->_ingredients = $ingredients;
        }
    }
    
    public function setPreparation($preparation){
        if(is_string($preparation)&& (strlen(utf8_decode($preparation))) <= 65535){
            $this->_preparation = $preparation;
        }
    }
    
    public function setDiet($diet) {
        if(is_array($diet)){
            $this->_diet = implode(", ", $diet);
        }
        if(is_string($diet)&& (strlen(utf8_decode($diet))) <= 65535){
            $this->_diet = $diet;
        }
    }
    
    public function setDifficulty_level($difficulty_level) {
        if(is_string($difficulty_level)&& (strlen(utf8_decode($difficulty_level))) <= 255){
            $this->_difficulty_level = $difficulty_level;
            switch ($difficulty_level) {
                case 'Très facile':
                    $this->_difficulty_level_numeric = 10;
                    break;
                
                case 'Facile':
                    $this->_difficulty_level_numeric = 25;
                    break;
                
                case 'Normal':
                    $this->_difficulty_level_numeric = 50;
                    break;
                
                case 'Difficile':
                    $this->_difficulty_level_numeric = 75;
                    break;
                
                case 'Très difficile':
                    $this->_difficulty_level_numeric = 100;
                    break;

                default:
                    $this->_difficulty_level_numeric = 50;
                    break;
            }
        }
    }
    
    public function setType_of_meal($type_of_meal){
        if(is_array($type_of_meal)){
            $this->_type_of_meal = implode(", ", $type_of_meal);
        }
        if(is_string($type_of_meal)&& (strlen(utf8_decode($type_of_meal))) <= 65535){
            $this->_type_of_meal = $type_of_meal;
        }
    }
    
    public function setNb_views($Nb_views = 0) {
        $nb_views = (int) $Nb_views;
        if($nb_views >= 0){
            $this->_nb_views = $nb_views;
        }
    }
    
    public function setNb_likes($Nb_likes = 0) {
        $nb_likes = (int) $Nb_likes;
        if($nb_likes >= 0){
            $this->_nb_likes = $nb_likes;
        }
    }
    
    public function setPreparation_time($Preparation_time = NULL) {
        $preparation_time = (int) $Preparation_time;
        if($preparation_time >= 0){
            $this->_preparation_time = $preparation_time;
        }
    }
    
    public function setPublication_date($publication_date = '') {
        if(is_string($publication_date)) {
            $format = 'Y-m-d H:i:s';	
            $datecheck = DateTime::createFromFormat($format, $publication_date); // If $publication_date is not a date it will return false
            if($datecheck){
                $datecheck = $datecheck->format($format);
                if($datecheck == $publication_date){
                        $this->_publication_date = $publication_date;
                } else {
                    $this->_publication_date = date("Y-m-d H:i:s");
                }
            }
        }
    }
    
    public function setUpdate_date($update_date = '') {
        if(is_string($update_date)) {
            $format = 'Y-m-d H:i:s';	
            $datecheck = DateTime::createFromFormat($format, $update_date);
            if($datecheck){
                $datecheck = $datecheck->format($format);
                if($datecheck == $update_date){
                        $this->_update_date = $update_date;
                }
            } else {
                $this->_update_date = date("Y-m-d H:i:s");
            }
        }
    }
    
    //---------------------------- FUNCTIONS ---------------------------------//
    //------------------------------------------------------------------------//
    
}
