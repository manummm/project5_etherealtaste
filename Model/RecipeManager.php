<?php

/**
 * Description of recipeManager
 *
 * @author Manu
 */
class RecipeManager extends Db {
    
    //------------------------------ CRUD ------------------------------------//
    //------------------------------------------------------------------------//
    
    // CREATE -------- Add a new recipe in the database ----------------------//
    public function addRecipe(Recipe $recipe) {
        // Define the SQL request
        $sql = 'INSERT INTO project5_recipe(name, author, ingredients, preparation, diet, difficulty_level, type_of_meal, nb_views, nb_likes, preparation_time, publication_date, update_date) '
             . 'VALUES(:name, :author, :ingredients, :preparaton, :diet, :difficulty_level, :type_of_meal, 0, 0, :preparation_time, NOW(), NOW())';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $name = $recipe->getName();
        $query->bindParam(':name', $name, PDO::PARAM_STR, 255);
        $author = $recipe->getAuthor();
        $query->bindParam(':author', $author, PDO::PARAM_STR, 255);
        $ingredients = $recipe->getIngredients();
        $query->bindParam(':ingredients', $ingredients, PDO::PARAM_STR);
        $preparation = $recipe->getPreparation();
        $query->bindParam(':preparaton', $preparation, PDO::PARAM_STR);
        $diet = $recipe->getDiet();
        $query->bindParam(':diet', $diet, PDO::PARAM_STR);
        $difficulty_level = $recipe->getDifficulty_level();
        $query->bindParam(':difficulty_level', $difficulty_level, PDO::PARAM_STR, 255);
        $type_of_meal = $recipe->getType_of_meal();
        $query->bindParam(':type_of_meal', $type_of_meal, PDO::PARAM_STR, 255);
        $preparation_time = $recipe->getPreparation_time();
        $query->bindParam(':preparation_time', $preparation_time, PDO::PARAM_STR);
        // Execute the query
        $query->execute();
        // Hydrate the Recipe Object
        $recipe->hydrate(array('id' => $this->lastId()));
        return TRUE;
    }
    
    // READ ------- Get a specific recipe from the database ------------------//
    public function getRecipe($id) {
        // Define the SQL request
        $sql = 'SELECT id, name, author, ingredients, preparation, diet, difficulty_level, type_of_meal, nb_views, nb_likes, preparation_time, publication_date, update_date '
             . 'FROM project5_recipe '
             . 'WHERE id = :id';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        // Look if there is a result & return it
        if ($query->rowCount() == 1){
            $data = $query->fetch();  // Access to the first result line
            return new Recipe($data);
        }else{
            throw new Exception("No recipe corresponds to the ID '$id'");
        }
    }
    
    // UPDATE ----- Modify a specific recipe in the database -----------------//
    public function updateRecipe(recipe $recipe) {
        // Define the SQL request
        $sql = 'UPDATE project5_recipe '
             . 'SET name = :name, author = :author, ingredients = :ingredients, preparation = :preparation, diet = :diet, difficulty_level = :difficulty_level, type_of_meal = :type_of_meal, nb_views = :nb_views, nb_likes = :nb_likes, preparation_time = :preparation_time, update_date = NOW() '
             . 'WHERE id = :id';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $name = $recipe->getName();
        $query->bindParam(':name', $name, PDO::PARAM_STR, 255);
        $author = $recipe->getAuthor();
        $query->bindParam(':author', $author, PDO::PARAM_STR, 255);
        $ingredients = $recipe->getIngredients();
        $query->bindParam(':ingredients', $ingredients, PDO::PARAM_STR);
        $preparation = $recipe->getPreparation();
        $query->bindParam(':preparation', $preparation, PDO::PARAM_STR);
        $diet = $recipe->getDiet();
        $query->bindParam(':diet', $diet, PDO::PARAM_STR);
        $difficulty_level = $recipe->getDifficulty_level();
        $query->bindParam(':difficulty_level', $difficulty_level, PDO::PARAM_STR);
        $type_of_meal = $recipe->getType_of_meal();
        $query->bindParam(':type_of_meal', $type_of_meal, PDO::PARAM_STR, 255);
        $nb_views = $recipe->getNb_views();
        $query->bindParam(':nb_views', $nb_views, PDO::PARAM_INT);
        $nb_likes = $recipe->getNb_likes();
        $query->bindParam(':nb_likes', $nb_likes, PDO::PARAM_INT);
        $preparation_time = $recipe->getPreparation_time();
        $query->bindParam(':preparation_time', $preparation_time, PDO::PARAM_STR);
        $id = $recipe->getId();
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        return TRUE;
    }
    
    // DELETE ----- Delete a specific recipe in the database -----------------//
    public function deleteRecipe($id) {
        $sql = 'DELETE FROM project5_recipe '
             . 'WHERE id = :id';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        // Return true as done
        return TRUE;
    }
    
    //------------------------------------------------------------------------//
    //------------------------------ END CRUD --------------------------------//
    
    
    // READ ALL ------- Get all the last recipes from the database -----------//
    public function getRecipes() {
        // Define a blank array to stock the query's answer
        $recipes = [];
        $sql = 'SELECT * '
             . 'FROM project5_recipe '
             . 'ORDER BY id DESC '  ;
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Execute the query
        $query->execute();
        // Complete & return the array
        while ($data = $query->fetch(PDO::FETCH_ASSOC)) {
            $recipes[] = new Recipe($data);
        }
        return $recipes;
    }
    
    // READ PART ------ Get the last 5 recipes from the database -------------//
    public function getPaginatedRecipes($page) {
        // Define a blank array to stock the query's answer
        $recipes = [];
        $recipesByPage = 5;
        $offset = ($page - 1) * $recipesByPage;
        $sql = 'SELECT * '
             . 'FROM project5_recipe '
             . 'ORDER BY id DESC '
             . 'LIMIT 5 OFFSET :offset '  ;
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':offset', $offset, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        // Complete & return the array
        while ($data = $query->fetch(PDO::FETCH_ASSOC)) {
            $recipes[] = new Recipe($data);
        }
        return $recipes;
    }
    
    // Get the number of recipes in the table
    public function getNumberOfRecipes() {
        $sql = 'SELECT COUNT(*) '
             . 'FROM project5_recipe ';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Execute the query
        $query->execute();
        // Number of rows
        $numberOfRecipes = $query->fetchColumn();
        return $numberOfRecipes;
    }
    
    //--------------------------- FUNCTIONS ----------------------------------//
    //------------------------------------------------------------------------//
    
    // Check if the recipe has been viewed by the user (if the line exists) & return TRUE if so, otherwise, return FALSE
    public function isViewed($userId, $recipeId) {
        // Define the SQL request
        $sql = 'SELECT * '
             . 'FROM project5_link_recipe_user '
             . 'WHERE id_user = :userId AND id_recipe = :recipeId';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':userId', $userId, PDO::PARAM_INT);
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        // Look if there is a result & if so, return TRUE, otherwise, return FALSE
        if ($query->rowCount() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    // Increment the views variable in the 'project5_link_recipe_user' table  by 1
    public function incrementUserViews($userId, $recipeId){
        // Define the SQL request
        $sql = 'UPDATE project5_link_recipe_user '
             . 'SET viewed = viewed + 1 '
             . 'WHERE id_user = :userId AND id_recipe = :recipeId';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':userId', $userId, PDO::PARAM_INT);
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        return TRUE;
    }
    
    // Increment the nb_views variable in the 'project5_recipe' table  by 1
    public function incrementViews($recipeId){
        // Define the SQL request
        $sql = 'UPDATE project5_recipe '
             . 'SET nb_views = nb_views + 1 '
             . 'WHERE id = :recipeId';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        return TRUE;
    }
    
    // Create the entry [user-recipe] in the link table
    public function addLinkUserRecipe($userId, $recipeId) {
        // Define the SQL request
        $sql = 'INSERT INTO project5_link_recipe_user(id_user, id_recipe, viewed) '
             . 'VALUES(:userId, :recipeId, :viewed)';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $viewed = 1;
        $query->bindParam(':viewed', $viewed, PDO::PARAM_INT);
        $query->bindParam(':userId', $userId, PDO::PARAM_INT);
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute(); 
        return TRUE;
    }
    
    // Check if the recipe has been liked by the user (if the line exists) & return TRUE if so, otherwise, return FALSE
    public function isLiked($userId, $recipeId) {
        // Define the SQL request
        $sql = 'SELECT liked '
             . 'FROM project5_link_recipe_user '
             . 'WHERE id_user = :userId AND id_recipe = :recipeId';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':userId', $userId, PDO::PARAM_INT);
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        // Look if there is a result & if so, check if it's " 1 " and then return TRUE, otherwise, return FALSE
        if ($query->rowCount() == 1){
            $result = $query->fetch(PDO::FETCH_ASSOC);
            if( $result['liked'] == 1){
                return TRUE;
            }
        }
        return FALSE;
    }
    
    // Set the liked variable from an entry [user-recipe] to 1
    public function userLikes($userId, $recipeId) {
        // Define the SQL request
        $sql = 'UPDATE project5_link_recipe_user '
             . 'SET liked =  :liked '
             . 'WHERE id_user = :userId AND id_recipe = :recipeId';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $liked = 1;
        $query->bindParam(':liked', $liked, PDO::PARAM_INT);
        $query->bindParam(':userId', $userId, PDO::PARAM_INT);
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute(); 
        return TRUE;
    }
    
    // Increment the nb_likes variable in the 'project5_recipe' table  by 1
    public function incrementLikes($recipeId){
        // Define the SQL request
        $sql = 'UPDATE project5_recipe '
             . 'SET nb_likes = nb_likes + 1 '
             . 'WHERE id = :recipeId';
        // Prepare the query
        $query = $this->getDb()->prepare($sql);
        // Bind parameters
        $query->bindParam(':recipeId', $recipeId, PDO::PARAM_INT);
        // Execute the query
        $query->execute();
        return TRUE;
    } 
    
}
