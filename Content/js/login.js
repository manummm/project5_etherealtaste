/* 
 * AJAX call for login
 */

$(document).ready(function(){
    $("#loginForm").on("submit", function(event){
        event.preventDefault();
        // Display a loader (CSS)
        $(".loaderOverlay").show();
        // Remove all the active errors
        $(this).find(".error").remove();
        // Remove all the active error focus (through the errorFocus class)
        $(this).find(".errorFocus").removeClass("errorFocus");
        // Declare variables ---------------------------------------------------
        username = $(this).find("input[name=username]").val();
        password = $(this).find("input[name=password]").val();
        rememberMe = $(this).find("input[name=rememberMe]:checked").val();
        usernameInput = $(this).find("input[name=username]");
        passwordInput = $(this).find("input[name=password]");
        url = $(this).attr("action"); // Value of the attribute "action"
        // ---------------------------------------------------------------------
        // Check if the two inputs aren't empty and then do an Ajax call in post
        if(username.trim() != '' && password.trim() != ''){
            $.post(url,{username:username,password:password,rememberMe:rememberMe}, function(data){
                // Formating for the view
                formattedUsername = username[0].toUpperCase() + username.slice(1).toLowerCase();
                // Hide the Loader (CSS)
                $(".loaderOverlay").fadeOut();
                if(data.state == "success"){
                    // Close the Login modal
                    $("#closeLogin").click();
                    // Display the name of the logged user
                    $("#loginState").html('<li class="nav-item"><a class="btn btn btn-outline-primary mr-2" href="index.php?action=profile"><i class="fa fa-user"></i><span id="username">' + formattedUsername + '</span></a></li>');
                    // Display the log out button
                    $("#loginState").append('<a id="logout" class="btn btn-outline-primary mr-2" href="index.php?action=logout"><i class="fas fa-power-off"></i>Se déconnecter</a>');
                    // Display a message to confirm the connection
                    $("#messageSuccess").html(data.return).fadeIn().delay(5000).fadeOut();
                }else if(data.state == "fail"){
                    // Display a error message under the password input
                    passwordInput.after('<p class="error" style="color:red; text-align:center; padding:10px; margin-bottom: -1rem;">' + data.return + '</p>');
                    // Put a red error focus the inputs to correct                        
                    usernameInput.addClass("errorFocus");
                    passwordInput.addClass("errorFocus");
                }
            }, "json");
        }else{
            if(username.trim() == ''){
                // Display a error message under the username input
                usernameInput.after('<p class="error">Veuillez renseigner l\'identifiant</p>');
                // Put the focus on the input to correct
                usernameInput.addClass("errorFocus");
            }else{
                // Display a error message under the password input
                passwordInput.after('<p class="error">Veuillez renseigner le mot de passe</p>');
                // Put the focus on the input to correct
                passwordInput.addClass("errorFocus");
            }
        }
    });    
});