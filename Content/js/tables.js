/* 
 * Charge the DataTables & its roperties when the document is readdy
 */

$(document).ready(function () {
    
    // Resize the table
    setTimeout(function(){
        $('#recipes').DataTable().columns.adjust().responsive.recalc();
    }, 200);
    
    // Instanciate the DataTable in the #recipes element
    $('#recipes').DataTable( {
        "autoWidth": true,
        "order": [[ 2, "desc" ]],
        "dom": '<<"#topRecipeTable"lf><t><"#bottomRecipeTable"ip>>',
        "responsive": true,
        "columnDefs": [
            {"name": "name", "responsivePriority": 1, "targets": 0},
            {"name": "author", "responsivePriority": 2, "targets": -1},
            {"name": "publicationdate", "responsivePriority": 3, "targets": 2},
            {"name": "action", "responsivePriority": 4, "targets": 1}
        ]
    } );
    
    // Add the classes "compact" & "nowrap" to the table #recipes
    $('#recipes').addClass("hover compact nowrap");
    
    // Recalculate the widths used by responsive & the column widths after a windows resizing
    $( window ).on('resize', function() {
        $('#recipes').DataTable().columns.adjust().responsive.recalc();
    });
        
});