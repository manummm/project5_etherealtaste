/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready( function () {
    $('.recipeControls').on('click', '#tabIngredients', function (event) {
        $('.articleRecipe .recipeIngredients').attr("style", "z-index: 51");
        $('#tabIngredients').addClass("activeTab").attr("style", "z-index: 52");
        $('.articleRecipe .recipePreparation').attr("style", "z-index: 50");
        $('#tabPreparation').removeClass("activeTab").attr("style", "z-index: 50");
    });
    $('.recipeControls').on('click', '#tabPreparation', function (event) {
        $('.articleRecipe .recipeIngredients').attr("style", "z-index: 50");
        $('#tabIngredients').removeClass("activeTab").attr("style", "z-index: 50");
        $('.articleRecipe .recipePreparation').attr("style", "z-index: 51");
        $('#tabPreparation').addClass("activeTab").attr("style", "z-index: 52");
    });
} );

