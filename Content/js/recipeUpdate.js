/* 
 * Update the data of the modal to fit if it's either a creation of a new recipe or an edition of an existing 
 */

function urldecode(encodedString){
    return decodeURIComponent(encodedString.replace(/\+/g, ' '));
}

// Event on the opening of the modal, set content in textarea if needed (if we edit a post)
$(document).ready(function(){
    $('#modalRecipe').on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget); // Button that triggered the modal
        // Extract info from data-* attributes
        const id = button.data('idrecipe');
        const modal = $(this);
        if(id.toString().length > 1){
            // Extract info from data-* attributes            
            const name = urldecode(button.data('namerecipe'));
            const author = urldecode(button.data('authorrecipe'));
            const ingredients = urldecode(button.data('ingredientsrecipe'));
            const preparation = urldecode(button.data('preparationrecipe'));
            const diet = (urldecode(button.data('dietrecipe'))).split(', ');
            const difficulty = urldecode(button.data('difficultyrecipe'));
            const typeofmeal = (urldecode(button.data('typeofmealrecipe'))).split(', ');
            const preparationtime = button.data('preparationtimerecipe');
            // Set the name of the modal windows to "Modification of [recipe to be modified]"
            modal.find('h4.modal-title').text('Modification de "' + name + '"');
            // Change the action link for updating a existing recipe and set the name of the submit button to "Modify"
            $("#recipeForm").attr( "action", "index.php?action=updateRecipe" );
            $("#recipeFormId").attr("value", id);
            $("#btn-submitRecipe").val("Modifier");
            // Set the values of the selected recipe in the form fields
            modal.find('#name').val(name);
            modal.find('#author').val(author);
            modal.find('#ingredients').val(ingredients);
            modal.find('#preparation').val(preparation);
            modal.find('input[name="diet[]"]').val(diet);
            modal.find("select[name=difficulty_level]").val(difficulty);
            modal.find('input[name="type_of_meal[]"]').val(typeofmeal);
            modal.find('#preparation_time').val(preparationtime);
        }else{
            // Set the name of the modal windows to "New recipe"
            modal.find('h4.modal-title').text('Nouvelle Recette');
            // Change the action link for adding a new recipe and set the name of the submit button to "Publish"
            $("#recipeForm").attr( "action", "index.php?action=addRecipe" );
            $("#btn-submitRecipe").val("Publier");
            // Reset the form fields to the default
            modal.find('#name').val("");
            modal.find('#author').val("");
            modal.find('#ingredients').val("");
            modal.find('#preparation').val("");
            modal.find('input[name="diet[]"]').val(["Végétarien"]);
            modal.find('input[name="type_of_meal[]"]').val([]);
            modal.find('#preparation_time').val("");
            modal.find("input:invalid").removeClass("input:invalid");
            modal.find("input:-moz-ui-invalid").removeClass("input:-moz-ui-invalid");
        }
    });
 });