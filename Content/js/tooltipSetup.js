/* 
 * Tooltip Setup
 */

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();    
});

$(document).ready(function(){
    $('.tooltip-add').tooltip({title: "Ajouter", placement: "top"});
    $('.tooltip-edit').tooltip({title: "Editer", placement: "top"});
    $('.tooltip-delete').tooltip({title: "Supprimer", placement: "top"});
});