/* 
 * AJAX call for logout
 */

$(document).ready(function(){
    $("#loginState").on("click", "#logout", function(event){
        event.preventDefault();
        var r = confirm("Souhaitez-vous vous déconnecter ?");
        if(r == false){
            return;
        }else{
            // Display a loader (CSS)
            $(".loaderOverlay").show();
            url = $(this).attr("href"); // Value of the attribute "action"
            $.post(url, function(data){
                // Hide the Loader (CSS)
                $(".loaderOverlay").fadeOut();
                if(data.state == "success"){
                    // Redirection
                    window.location.replace(data.homepage);
                }else{
                    $("#messageSuccess").html(data.return).fadeIn().delay(10000).fadeOut();
                }
            }, "json");
        }
    });
    // Check if there were a redirecion from a logout and if so, show a message
    var searchParams = new URLSearchParams(window.location.search);
    if(searchParams.has("logout")){
        var username = searchParams.get("username");
        var message = '<strong>Vous êtes maintenant déconnecté ! Au revoir '+username+' !</strong>';
        // Display a message to confirm the deconnection
        $("#messageSuccess").html(message).fadeIn().delay(5000).fadeOut();
        // Rewrite the URL
        var currentURL = window.location.href.split("?")[0];
        window.history.replaceState({}, "", currentURL);
    }
});