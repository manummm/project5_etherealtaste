/* 
 * Ajax call to create or edit a recipe
 */

$(document).ready(function(){
    // Ajax new & edit Recipe
    $("#recipeForm").on("submit", function(event){
        event.preventDefault();
        // Display a loader (CSS)
        $(".loaderOverlay").show();
        // Remove all the active errors
        $(this).find(".error").remove();
        // Remove all the active error focus (through the errorFocus class)
        $(this).find(".errorFocus").removeClass("errorFocus");
        
        // --------------------- Declaring variables ---------------------------

        // Get the values to be transmitted within the ajax call
        const id = $(this).find("input[name=id]").val();
        const name = $(this).find("input[name=name]").val();
        const author = $(this).find("input[name=author]").val();
        const ingredients = $(this).find("textarea[name=ingredients]").val();
        const preparation = $(this).find("textarea[name=preparation]").val();
        const diet = [];
        $.each($("input[name='diet[]']:checked"), function(){
            diet.push($(this).val());
        });
        const difficulty_level = $(this).find("select[name=difficulty_level]").val();
        const type_of_meal = [];
        $.each($("input[name='type_of_meal[]']:checked"), function(){
            type_of_meal.push($(this).val());
        });
        const preparation_time = $(this).find("input[name=preparation_time]").val();
        const url = $(this).attr("action");
        
        // Get the fields of the form for the error handling -------------------
        const nameInput = $(this).find("input[name=name]");
        const authorInput = $(this).find("input[name=author]");
        const ingredientsInput = $(this).find("textarea[name=ingredients]");
        const preparationInput = $(this).find("textarea[name=preparation]");
        const dietInput = $(this).find("input[name=diet]");
        const difficulty_levelInput = $(this).find("select[name=difficulty_level]");
        const type_of_mealInput = $("#dietField");
        const preparation_timeInput = $(this).find("input[name=preparation_time]");
        
        // ---------------------------------------------------------------------

        // Check if the inputs aren't empty and then do the Ajax call to the specified Url
        if(name && author && ingredients && preparation && preparation_time && type_of_meal.length !== 0 && url){
            $.post(url,{"id": id, "name": name, "author": author, "ingredients": ingredients, "preparation": preparation, "diet": diet, "difficulty_level": difficulty_level, "type_of_meal": type_of_meal, "preparation_time": preparation_time}, function(data){
                // Hide the Loader (CSS)
                $(".loaderOverlay").fadeOut();
                if(data.state == "add"){
                    // Close the recipe modal
                    $("#closeRecipe").click();
                    // Add the line for the new recipe in the table
//                    $("#listRecipes").prepend(data.datanewline);
//                    // Remove the last line in the table
//                    $("#listRecipes tr").last().remove();
                    var newRow = data.newline;
                    var table = $('#recipes').DataTable();
                    table.row.add($(newRow)).draw();
                    // Add the tooltips to the 'edit' & 'delete' icons
                    $('.tooltip-edit').tooltip({title: "Editer"});
                    $('.tooltip-delete').tooltip({title: "Supprimer"});
                    // Display a message to confirm the connection
                    $("#messageSuccess").html(data.return).fadeIn().delay(5000).fadeOut();
                }else if(data.state == "edit"){
                    // Close the recipe modal
                    $("#closeRecipe").click();
                    // Add the line for the of the updated recipe in the table
                    // $("#" + data.linetoedit).after(data.datanewline);
                    // Remove the old line which was edited from the table
                    // $("#" + data.linetoedit).remove();
                    var newRow = data.newline;
                    var table = $('#recipes').DataTable();
                    table.row.add($(newRow)).draw();
                    // Remove the old line which was edited from the table
                    $('#recipes').DataTable().rows("#" + data.linetoedit).remove().draw();
                    // Add the tooltips to the 'edit' & 'delete' icons
                    $('.tooltip-edit').tooltip({title: "Editer"});
                    $('.tooltip-delete').tooltip({title: "Supprimer"});
                    // Display a message to confirm the connection
                    $("#messageSuccess").html(data.return).fadeIn().delay(5000).fadeOut();
                }else{
                    $("#messageFail").html(data.return).fadeIn().delay(5000).fadeOut();
                }
            }, "json");
        }else{
            // Hide the Loader (CSS)
            $(".loaderOverlay").fadeOut();
            if(name.trim() == ''){
                // Display a error message under the title input
                nameInput.after('<p class="error">Veuillez renseigner le nom de la recette</p>');
                // Put the focus on the input to correct
                nameInput.addClass("errorFocus");
            }else if(author.trim() == ''){
                // Display a error message under the content input
                authorInput.after('<p class="error">Veuillez renseigner l\'auteur de la recette</p>');
                // Put the focus on the input to correct
                authorInput.addClass("errorFocus");
            }else if(ingredients.trim() == ''){
                // Display a error message under the content input
                ingredientsInput.after('<p class="error">Veuillez renseigner les ingrédients de la recette</p>');
                // Put the focus on the input to correct
                ingredientsInput.addClass("errorFocus");
            }else if(preparation.trim() == ''){
                // Display a error message under the content input
                preparationInput.after('<p class="error">Veuillez renseigner la preparation de la recette</p>');
                // Put the focus on the input to correct
                preparationInput.addClass("errorFocus");
            }else if(preparation_time.trim() == ''){
                // Display a error message under the content input
                preparation_timeInput.after('<p class="error">Veuillez renseigner le temps de préparation de la recette</p>');
                // Put the focus on the input to correct
                preparation_timeInput.addClass("errorFocus");
            }else if(type_of_meal.length === 0){
                // Display a error message under the content input
                type_of_mealInput.after('<p class="error">Veuillez renseigner le type de repas de la recette</p>');
                // Put the focus on the input to correct
                type_of_mealInput.addClass("errorFocus");
            }
        }
    });
});


