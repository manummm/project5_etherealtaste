/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Function to return the value of a specied cookie
function getCookie(name) {
    // Variable "nameCookie="
    var nameEQ = name + "=";
    // Decode the cookie string to handle cookies with special characters
    var decodedCookie = decodeURIComponent(document.cookie);
    // Define an array populated with all the cookies that are set for this domain & path
    var ca = decodedCookie.split(';');
    // Loop throught the array
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        // Removing space till the first character isn't a space
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        // Looking for a cookie "namecookie=valuecookie" starting by nameEQ "namecookie=" with indexOf
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length); // If it exists, return the cookie value
        }
    }
    return null; //If the coookie doesn't exist, return null
}


// Event on click on the arrows-down, scroll the viewport till the #global
$(document).ready(function(){
    $('#accueil').on('click', '.arrow', function (event){
        $('html, body').animate({
            scrollTop: $('#global').offset().top
        }, 500);
    });
    
    if($('#accueil').length > 0){
        x = getCookie('skip_accueil');
        if(x){
            if(x > 1){
                $('html, body').animate({
                    scrollTop: $('#global').offset().top
                }, 100);
            }
        }
    }
    
 });
