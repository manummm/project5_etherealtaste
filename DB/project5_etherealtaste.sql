-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 28 août 2019 à 23:25
-- Version du serveur :  10.1.31-MariaDB
-- Version de PHP :  7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `project5_etherealtaste`
--

-- --------------------------------------------------------

--
-- Structure de la table `project5_link_recipe_user`
--

CREATE TABLE `project5_link_recipe_user` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_recipe` int(11) NOT NULL,
  `viewed` int(11) NOT NULL,
  `liked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `project5_link_recipe_user`
--

INSERT INTO `project5_link_recipe_user` (`id`, `id_user`, `id_recipe`, `viewed`, `liked`) VALUES
(21, 1, 80, 21, 1),
(23, 1, 79, 4, 0),
(24, 1, 74, 1, 0),
(25, 1, 76, 1, 0),
(26, 1, 89, 1, 0),
(27, 1, 100, 1, 0),
(28, 1, 116, 1, 0),
(29, 1, 115, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `project5_recipe`
--

CREATE TABLE `project5_recipe` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `ingredients` text NOT NULL,
  `preparation` text NOT NULL,
  `diet` varchar(255) NOT NULL,
  `difficulty_level` varchar(255) NOT NULL,
  `type_of_meal` varchar(255) NOT NULL,
  `nb_views` int(11) NOT NULL,
  `nb_likes` int(11) NOT NULL,
  `preparation_time` int(11) NOT NULL,
  `publication_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `project5_recipe`
--

INSERT INTO `project5_recipe` (`id`, `name`, `author`, `ingredients`, `preparation`, `diet`, `difficulty_level`, `type_of_meal`, `nb_views`, `nb_likes`, `preparation_time`, `publication_date`, `update_date`) VALUES
(1, 'Tomates farcies', 'Manu', 'Tomates lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '1', '2', 'main course', 245, 56, 3600, '2019-02-01 00:00:00', '2019-02-01 00:00:00'),
(2, 'Riz cantonais', 'Roger', 'Riz, petits pois, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum\r\n', 'Rincer le riz à l\'eau froide, lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '2', '5', 'main course', 68, 34, 2400, '2019-02-16 09:30:32', '2019-02-16 09:30:32'),
(3, 'Soupe à l\'oignon', 'Jade', 'Oignons, huile de coco, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum\r\n', 'Emincer les oignons, lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '4', '7', 'main course', 77, 56, 4800, '2019-02-21 09:10:32', '2019-02-23 15:51:08'),
(6, 'Oignon frits vvv', 'Manu', 'hhhh', 'jjjj', '0', '0', 'Diner', 0, 0, 342, '2019-04-08 13:56:23', '2019-04-08 13:56:23'),
(7, 'test', 'Manu', 'yyy', 'pp', '0', '0', 'Petit Déjeuner', 0, 0, 78, '2019-04-08 13:59:49', '2019-04-08 13:59:49'),
(8, 'x', 'Manu', '  x', 'x', '0', '0', 'Petit Déjeuner', 0, 0, 85, '2019-04-08 14:00:14', '2019-04-08 14:00:14'),
(11, 'Tomates spilper', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 64, '2019-04-11 14:02:04', '2019-04-11 14:02:04'),
(18, 'Poireaux hollandais', 'Manu', 'ccc', 'ccc', '0', '0', 'Petit Déjeuner', 0, 0, 69, '2019-04-11 14:27:31', '2019-04-11 14:27:31'),
(28, 'Articoke smoked', 'Manu', '', '', 'Végétarien', 'Facile', 'Plat principal', 0, 0, 6589, '2019-04-15 12:50:33', '2019-07-23 21:46:10'),
(30, 'Lasagne de courgette', 'Manu', 'Courgetes loremipsuuumu', 'ccc', '0', '0', 'Déjeuner', 0, 0, 60, '2019-04-15 12:52:48', '2019-04-15 12:52:48'),
(33, 'test ', 'ggggg', 'gg', 'ggg', '0', '0', 'Petit Déjeuner', 0, 0, 0, '2019-04-15 13:05:31', '2019-04-15 13:05:31'),
(34, 'test2', 'Manu', '8888', '888', '0', '0', 'Diner', 0, 0, 888, '2019-04-15 13:14:42', '2019-04-15 13:14:42'),
(35, 'test3', 'Manu', 'ccc', 'ccc', '0', '0', 'Petit Déjeuner', 0, 0, 0, '2019-04-15 13:17:46', '2019-04-15 13:17:46'),
(36, 'test4', 'ddd', 'dd', 'dd', '0', '0', 'Petit Déjeuner', 0, 0, 78, '2019-04-15 13:22:50', '2019-04-15 13:22:50'),
(38, 'test', 'Manu', 'ccc', 'yy', '0', '0', 'Petit Déjeuner', 0, 0, 85, '2019-04-22 13:21:30', '2019-04-22 13:21:30'),
(39, 'Test88', 'xxx', 'xxx', 'xx', '0', '0', 'Petit Déjeuner', 0, 0, 0, '2019-04-22 13:22:22', '2019-04-22 13:22:22'),
(40, 'yyyy', 'yyy', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 66, '2019-04-22 13:23:06', '2019-04-22 13:23:06'),
(42, 'POis', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 666, '2019-04-22 13:25:22', '2019-04-22 13:25:22'),
(46, 'Oignon frits', 'ffff', 'f', '', '0', '0', 'Petit Déjeuner', 0, 0, 888, '2019-04-29 11:41:39', '2019-04-29 11:41:39'),
(47, 'rt', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 987, '2019-04-29 11:42:29', '2019-04-29 11:42:29'),
(48, 'test666', 'El diablo', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 666, '2019-04-29 11:53:58', '2019-04-29 11:53:58'),
(49, 'Tomates spilper', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 2147483647, '2019-04-29 11:55:50', '2019-04-29 11:55:50'),
(50, 'x', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 2147483647, '2019-04-29 11:57:23', '2019-04-29 11:57:23'),
(51, 'Oignon fritstttttttttt', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 2147483647, '2019-04-29 12:05:47', '2019-04-29 12:05:47'),
(53, 'Beignets', 'TTT', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 2147483647, '2019-04-29 12:07:43', '2019-04-29 12:07:43'),
(54, 'Oignon frits', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 2147483647, '2019-04-29 12:13:21', '2019-04-29 12:13:21'),
(55, 'Bananas', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 666, '2019-04-29 12:14:20', '2019-04-29 12:14:20'),
(57, 'Abricots', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 666, '2019-04-29 12:17:32', '2019-04-29 12:17:32'),
(58, 'Articoke v2', 'Manu', 'gfgfgf', 'gfgf', 'Végétarien', 'Normal', 'Boisson, Salade', 0, 0, 98563, '2019-04-29 12:27:52', '2019-07-23 21:46:31'),
(61, 'Oignon frits', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 999, '2019-04-29 12:42:55', '2019-04-29 12:42:55'),
(62, 'Tiramisu', 'Matshiba', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 655957, '2019-04-29 12:44:16', '2019-04-29 12:44:16'),
(63, 'Légumes rôtis', 'Hector', '', '', '0', '0', 'Petit Déjeuner', 1, 0, 4521, '2019-04-29 12:46:13', '2019-04-29 12:46:13'),
(64, 'Pancakes', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 357, '2019-04-29 12:46:57', '2019-04-29 12:46:57'),
(68, 'Minestrone', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 33, '2019-05-09 20:13:58', '2019-05-09 20:13:58'),
(70, 'Oignon frits v2', 'bbbrrr', '', '', '0', '0', 'Petit Déjeuner', 0, 0, 0, '2019-05-10 22:22:16', '2019-05-10 22:22:36'),
(71, 'Pollente v2', 'Manu', '', '', '0', '0', 'Petit Déjeuner', 1, 0, 666, '2019-05-10 22:50:12', '2019-05-10 22:51:16'),
(73, 'Oignon frits smoked', 'Manu', 'fff', 'fff', 'Végétarien, Sans oeufs', '0', 'Boisson, Salade', 0, 0, 7514, '2019-05-25 15:01:42', '2019-05-25 15:01:42'),
(74, 'Oignon frits', 'Manu', 'xxx', 'xxx', 'Végétarien, Sans lactose', '0', 'Dessert, Plat principal', 2, 0, 777, '2019-05-26 10:37:11', '2019-05-26 10:37:11'),
(76, 'Tagliatelles v2', 'Manu', '', '', 'Végétarien', 'Difficile', 'Plat principal', 1, 0, 784, '2019-05-26 10:44:48', '2019-07-23 21:47:08'),
(79, 'Articoke gelifié v2.20', 'Manu', 'test', '', 'Végétarien, Végétalien, Sans lactose, Sans gluten, Sans oeufs', 'Difficile', 'Boisson, En-cas, Plat principal, Salade', 0, 0, 777, '2019-05-26 11:15:54', '2019-08-01 19:00:30'),
(80, 'Zeste orangés v1.4', 'Manu v3', 'Oranges, sucre', 'Pesez les oranges', 'Végétarien, Sans oeufs', 'Normal', 'En-cas, Plat principal', 0, 0, 78, '2019-07-08 21:27:54', '2019-08-01 19:41:13'),
(89, 'oooo', 'iiii', 'iiii', 'iii', 'Végétarien, Sans lactose', 'Difficile', 'Plat principal', 1, 0, 74, '2019-08-08 23:56:11', '2019-08-08 23:56:11'),
(90, 'Pollenta Atomato', 'Manu', 'Pollente\nTomate', 'Cuire la pollente\nEplucher les tomates', 'Végétarien, Végétarien, Végétalien, Sans gluten', 'Indéfinie', 'Plat principal', 0, 0, 15, '2019-08-10 19:05:56', '2019-08-10 19:05:56'),
(98, '666ooooo', '999', '99', '999', 'Végétarien, Végétarien', 'Très facile', 'Plat principal', 0, 0, 546, '2019-08-10 19:31:17', '2019-08-10 19:31:17'),
(99, 'ttt', 'yyy', 'erzr', 'trtrz', 'Végétarien, Végétarien', 'Difficile', 'Plat principal', 0, 0, 492, '2019-08-10 19:34:50', '2019-08-10 19:34:50'),
(100, 'MMMMMMMMMy', 'pp', 'ppp', 'ppp', 'Végétarien, Végétalien, Sans lactose, Sans gluten, Sans oeufs', 'Facile', 'Boisson, En-cas, Dessert, Plat principal, Salade', 0, 0, 9658, '2019-08-11 12:53:07', '2019-08-16 22:33:11'),
(103, 'tttt', 'tttt', 'pppp', 'pppp', 'Végétarien, Sans lactose', 'Normal', 'En-cas', 0, 0, 8952, '2019-08-16 22:39:58', '2019-08-16 22:39:58'),
(107, 'hnhhhhhnnnhnhnhnhnhnhn', 'hn', 'hn', 'hn', 'Végétarien, Sans gluten', 'Très facile', 'En-cas, Salade', 0, 0, 13, '2019-08-17 21:01:55', '2019-08-17 21:01:55'),
(108, 'fafa', 'fafa', 'fff', 'ff', 'Végétarien, Sans oeufs', 'Difficile', 'Salade', 0, 0, 96, '2019-08-17 21:03:41', '2019-08-17 21:03:41'),
(111, 'ROOT Food', 'ROOT Food', 'ROOT Food', 'ROOT Food', 'Végétarien, Sans lactose, Sans oeufs', 'Indéfinie', 'Dessert, Plat principal', 0, 0, 444, '2019-08-20 17:21:08', '2019-08-20 17:21:08'),
(112, 'yyyyyyy', 'yyyy', 'yyy', 'yyy', 'Végétarien, Sans gluten', 'Normal', 'Salade', 0, 0, 1754, '2019-08-20 19:04:03', '2019-08-20 19:04:03'),
(113, 'YEAH ALL WORK', 'ROOT', 'rrrr', 'rrrr', 'Végétarien, Sans lactose, Sans oeufs', 'Difficile', 'Dessert, Plat principal', 0, 0, 1337, '2019-08-20 19:09:14', '2019-08-20 19:09:14'),
(114, 'test666', 'ffff', 'fff', 'fff', 'Végétarien, Sans oeufs', 'Très facile', 'Dessert, Plat principal', 0, 0, 78921456, '2019-08-20 19:12:09', '2019-08-20 19:12:09'),
(115, 'Ohééééééééé', 'Noé', 'eeee', 'rrrr', 'Végétarien, Sans gluten', 'Indéfinie', 'Boisson', 1, 0, 635, '2019-08-20 19:45:16', '2019-08-20 19:45:16'),
(116, 'Yoooo', 'Echo', 'fff', 'ooo', 'Végétarien, Sans oeufs', 'Difficile', 'Salade', 1, 0, 963, '2019-08-20 19:46:02', '2019-08-20 19:46:02'),
(117, 'TOMOTO', 'TY', 'gggg', 'ggg', 'Végétarien, Sans lactose', 'Très difficile', 'Dessert', 0, 0, 9856, '2019-08-20 23:06:26', '2019-08-20 23:06:26'),
(118, 'ffff', 'fff', 'fff', 'fffff', 'Végétarien, Végétalien', 'Facile', 'Salade', 0, 0, 99999, '2019-08-20 23:07:15', '2019-08-20 23:07:15'),
(119, 'TUU', 'ggg', 'ggg', 'gg', 'Végétarien, Sans lactose', 'Normal', 'En-cas', 0, 0, 75254, '2019-08-20 23:17:01', '2019-08-20 23:17:01');

-- --------------------------------------------------------

--
-- Structure de la table `project5_users`
--

CREATE TABLE `project5_users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `inscription_date` date NOT NULL,
  `token_session` varchar(255) NOT NULL,
  `accreditation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `project5_users`
--

INSERT INTO `project5_users` (`id`, `username`, `password_hash`, `email`, `inscription_date`, `token_session`, `accreditation_id`) VALUES
(1, 'superAdmin', '$2y$10$EnIlhDhNyJDc9kfCThED5Owht79dCE5szUQE64r6Ey5Ov2nHLjbdi', 'serve.emmanuel@gmail.com', '2019-03-11', '$2y$10$7GH19BUkcdRQpUEG7w39QO1q4EYm20dJE0JUe18SZKKQFGTmbKcZ.', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `project5_link_recipe_user`
--
ALTER TABLE `project5_link_recipe_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_actions` (`id_user`),
  ADD KEY `fk_recipe_actions` (`id_recipe`);

--
-- Index pour la table `project5_recipe`
--
ALTER TABLE `project5_recipe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `project5_users`
--
ALTER TABLE `project5_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `project5_link_recipe_user`
--
ALTER TABLE `project5_link_recipe_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `project5_recipe`
--
ALTER TABLE `project5_recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT pour la table `project5_users`
--
ALTER TABLE `project5_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `project5_link_recipe_user`
--
ALTER TABLE `project5_link_recipe_user`
  ADD CONSTRAINT `fk_recipe_actions` FOREIGN KEY (`id_recipe`) REFERENCES `project5_recipe` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_actions` FOREIGN KEY (`id_user`) REFERENCES `project5_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
