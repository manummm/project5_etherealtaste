<?php $this->title = "Etherealtase | " . filter_var($_SESSION['session']['username'], FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>

<h1>Profile</h1>

<!-- Administration - Recipe -->
<?php if(isset($_SESSION['session']['accreditation']) && $_SESSION['session']['accreditation'] == 1){ ?>
<section class="panel">
    <h2>Recettes</h2>
    <a href="#modalRecipe" id="addRecipe" class="btn btn-outline-success tooltip-add" data-toggle="modal"  data-idrecipe="" data-namerecipe="" data-authorrecipe=""><i class="fas fa-plus-circle"></i> Ecrire une nouvelle recette</a>
    <table id="recipes" class="recipes table-stripped">
        <thead>
            <th>Recette</th>
            <th>Auteur</th>
            <th>Date de publication</th>
            <th>Action</th>
        </thead>
        <tbody id="listRecipes">
            <?php foreach ($recipes as $recipe): ?>
            <tr id="<?= 'line' . filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>">
                <td>
                    <a href="<?= "index.php?action=getRecipe&id=" . filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>"><?= filter_var($recipe->getName(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></a>
                </td>
                <td>
                    <p><?= filter_var($recipe->getAuthor(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></p>
                </td>
                <td data-order="<?= filter_var($recipe->getPublication_date(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>">
                    <time><?= filter_var($recipe->getOptimizedPublicationDate(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></time>
                </td>
                <td>
                    <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#modalRecipe" data-idrecipe="<?= filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>" data-namerecipe="<?= filter_var(urlencode($recipe->getName()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-authorrecipe="<?= filter_var(urlencode($recipe->getAuthor()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-ingredientsrecipe="<?= filter_var(urlencode($recipe->getIngredients()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-preparationrecipe="<?= filter_var(urlencode($recipe->getPreparation()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-dietrecipe="<?= filter_var(urlencode($recipe->getDiet()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-difficultyrecipe="<?= filter_var(urlencode($recipe->getDifficulty_level()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-typeofmealrecipe="<?= filter_var(urlencode($recipe->getType_of_meal()), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>" data-preparationtimerecipe="<?= filter_var($recipe->getPreparation_time(), FILTER_SANITIZE_NUMBER_INT); ?>"><i class="fas fa-edit tooltip-edit"></i></button>
                    <form id="<?= 'deleteRecipe' . filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>" class="deleteRecipe" action="index.php?action=deleteRecipe" method="post">
                        <input type="hidden" name="id" value="<?= filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>" />
                        <button type="submit" class="btn btn-outline-danger"><i class="fas fa-trash-alt tooltip-delete"></i></button>
                    </form>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>
<!-- Modal new recipe -->
<div id="modalRecipe" class="modal fade">        
    <div class="modal-dialog modal-recipe">
        <div class="modal-content">
            <div class="modal-header">				
                <h4 class="modal-title">Nouvelle Recette</h4>
                <button id="closeRecipe" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php $form = new Form($_POST); ?>
                <form id="recipeForm" action="index.php?action=addRecipe" method="post">
                    <div class="form-group form-title">
                        <i class="fas fa-pencil-alt"></i>
                        <?php echo $form->input('text', ['name' => 'name', 'placeholder' => 'Nom', 'required' => 'required', 'id' => 'name', 'class' => 'form-control modal-title']); ?>
                    </div>
                    <div class="form-group form-title">
                        <i class="fas fa-pencil-alt"></i>
                        <?php echo $form->input('text', ['name' => 'author', 'placeholder' => 'Auteur', 'required' => 'required', 'id' => 'author', 'class' => 'form-control modal-title']); ?>
                    </div>
                    <div class="form-group form-title">
                        <i class="fas fa-pencil-alt"></i>
                        <?php echo $form->textarea(['name' => 'ingredients', 'placeholder' => 'Liste des ingrédients', 'id' => 'ingredients', 'rows' => '10', 'cols' => '50', 'class' => 'form-control']); ?>
                    </div>
                    <div class="form-group form-title">
                        <i class="fas fa-pencil-alt"></i>
                        <?php echo $form->textarea(['name' => 'preparation', 'placeholder' => 'Préparation', 'id' => 'preparation', 'rows' => '10', 'cols' => '50', 'class' => 'form-control']); ?>
                    </div>
                    <fieldset form="recipeForm">
                        <legend class="modal-recipe-legend"><h2>Diet</h2></legend>
                        <div class="modal-recipe-fieldset-innerwrapper">
                            <?php
                                echo $form->label('Végétarien', ['for' => 'vegetarian']);
                                echo $form->input('checkbox', ['name' => 'diet[]', 'value' => 'Végétarien', 'checked' => 'checked', 'id' => 'vegetarian', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'vegetarian', 'class' => 'switch']);
                                echo $form->label('Végétalien', ['for' => 'vegetalien']);
                                echo $form->input('checkbox', ['name' => 'diet[]', 'value' => 'Végétalien', 'id' => 'vegetalien', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'vegetalien', 'class' => 'switch']);
                                echo $form->label('Sans lactose', ['for' => 'lactosefree']);
                                echo $form->input('checkbox', ['name' => 'diet[]', 'value' => 'Sans lactose', 'id' => 'lactosefree', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'lactosefree', 'class' => 'switch']);
                                echo $form->label('Sans gluten', ['for' => 'glutenfree']);
                                echo $form->input('checkbox', ['name' => 'diet[]', 'value' => 'Sans gluten', 'id' => 'glutenfree', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'glutenfree', 'class' => 'switch']);
                                echo $form->label('Sans oeufs', ['for' => 'eggsfree']);
                                echo $form->input('checkbox', ['name' => 'diet[]', 'value' => 'Sans oeufs', 'id' => 'eggsfree', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'eggsfree', 'class' => 'switch']);
                            ?>
                        </div>
                    </fieldset>
                    <div class="form-group form-title">
                        <h2>Difficulté</h2>
                        <?php echo $form->select([['value' => 'Indéfinie', 'selected' => 'novalue'],['value' => 'Très facile'],['value' => 'Facile'],['value' => 'Normal'],['value' => 'Difficile'],['value' => 'Très difficile']],['name' => 'difficulty_level'],['id' => 'difficulty_level']) ?>
                    </div>
                    <fieldset form="recipeForm" id="dietField">
                        <legend class="modal-recipe-legend"><h2>Type de recette</h2></legend>
                        <div class="modal-recipe-fieldset-innerwrapper">
                            <?php
                                echo $form->label('Boisson', ['for' => 'drink']);
                                echo $form->input('checkbox', ['name' => 'type_of_meal[]', 'value' => 'Boisson', 'id' => 'drink', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'drink', 'class' => 'switch']);
                                echo $form->label('En-cas', ['for' => 'snack']);
                                echo $form->input('checkbox', ['name' => 'type_of_meal[]', 'value' => 'En-cas', 'id' => 'snack', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'snack', 'class' => 'switch']);
                                echo $form->label('Dessert', ['for' => 'dessert']);
                                echo $form->input('checkbox', ['name' => 'type_of_meal[]', 'value' => 'Dessert', 'id' => 'dessert', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'dessert', 'class' => 'switch']);
                                echo $form->label('Plat principal', ['for' => 'maindish']);
                                echo $form->input('checkbox', ['name' => 'type_of_meal[]', 'value' => 'Plat principal', 'id' => 'maindish', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'maindish', 'class' => 'switch']);
                                echo $form->label('Salade', ['for' => 'salad']);
                                echo $form->input('checkbox', ['name' => 'type_of_meal[]', 'value' => 'Salade', 'id' => 'salad', 'class' => 'offscreen']);
                                echo $form->label('', ['for' => 'salad', 'class' => 'switch']);
                            ?>
                        </div>
                    </fieldset>
                    
                    <div class="form-group form-title">
                        <i class="fas fa-pencil-alt"></i>
                        <?php echo $form->input('text', ['name' => 'preparation_time', 'placeholder' => 'Temps de préparation', 'id' => 'preparation_time', 'required' => 'required', 'id' => 'preparation_time', 'class' => 'form-control modal-title']); ?>
                    </div>
                    <div class="form-group">
                        <input id="recipeFormId" type="hidden" name="id" value="" />
                        <input type="submit" id="btn-submitRecipe" class="btn btn-primary btn-block btn-lg" value="Publier">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- End modal new recipe -->
<?php } ?>

<!-- Administration - Comment -->
<?php if(isset($_SESSION['session']['accreditation']) && $_SESSION['session']['accreditation'] == 1){ ?>
<section class="panel">
    <h2>Commentaires</h2>
</section>
<?php } ?>

<!-- Administration - User -->
<?php if(isset($_SESSION['session']['accreditation']) && $_SESSION['session']['accreditation'] == 1){ ?>
<section class="panel">
    <h2>Utilisateurs</h2>
</section>
<?php } ?>