<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author Manu
 */
class Form {
    
    private $_data;
    private $_surroundingTag;

    public function __construct($data = array()) {
        $this->_data = $data;
        $this->setSurrendingTag();
    }
    
    public function setSurrendingTag($surrounding_tag = '') {
        if(is_string($surrounding_tag) && (strlen(utf8_decode($surrounding_tag))) <= 30){
            $this->_surroundingTag = $surrounding_tag;
        }
    }
    
    /*
     * Return the html selection surrounded by the chosen tag
     */
    private function surround($html) {
        if($this->_surroundingTag === ''){
            $result = $html;
        } else {
            $result = "<{$this->_surroundingTag} class='form-group'>{$html}</{$this->_surroundingTag}>";
        }
        return $result;
    }
    
    /*
     * Return the _data[$name] if it exists & isn't NULL, otherwise return NULL
     */
    private function getValue($index) {
        $value = $this->_data[$index] ?? NULL;
        return filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }
    
    private function assignAttribute(string $attribute, array $attributes) {
        $attributeValue = $attributes[$attribute] ?? NULL;
        // Case where data are send with the new form
        if ($attribute === 'value' && isset($attributes['name']) && ($this->getValue($attributes['name']))) {
            return ($attribute.'="'.trim($this->getValue($attributes['name'])).'"');
        }
        if($attributeValue !== NULL){
            if($attributeValue === "novalue"){
                return $attribute;
            }
            return ($attribute.'="'.$attributeValue.'"');
        } else {
            return NULL;
        }
    }

    // FORM ELEMENTS -----------------------------------------------------------
    // -------------------------------------------------------------------------
    public function input(string $type, array $attributes = []) {
        
        $name = $this->assignAttribute('name', $attributes);
        $autocomplete = $this->assignAttribute('autocomplete', $attributes); // "on" or "off"
        $autofocus = $this->assignAttribute('autofocus', $attributes); // "autofocus" or nothing
        $checked = $this->assignAttribute('checked', $attributes); // "checked" or nothing
        $disabled = $this->assignAttribute('disabled', $attributes); // "disabled" or nothing
        $pattern = $this->assignAttribute('pattern', $attributes); // Regexp
        $placeholder = $this->assignAttribute('placeholder', $attributes); // Text
        $readonly = $this->assignAttribute('readonly', $attributes); // "readonly" or nothing
        $required = $this->assignAttribute('required', $attributes); // "required" or nothing
        $value = $this->assignAttribute('value', $attributes); // Text
        $id = $this->assignAttribute('id', $attributes);
        $class = $this->assignAttribute('class', $attributes);
        
        switch ($type) {                        
            case 'checkbox':
                return $this->surround('<input type="checkbox" '.$name.' '.$id.' '.$class.' '.$value.' '.$placeholder.' '.$pattern.' '.$autocomplete.' '.$autofocus.' '.$checked.' '.$disabled.' '.$readonly.' '.$required.'>');
                break;
            
            case 'hidden':
                return $this->surround('<input type="hidden" '.$id.' '.$name.' '.$value.'">');
                break;
            
            case 'password':
                return $this->surround('<input type="password" '.$name.' '.$id.' '.$class.' '.$value.' '.$placeholder.' '.$pattern.' '.$autocomplete.' '.$autofocus.' '.$checked.' '.$disabled.' '.$readonly.' '.$required.'>');
                break;
            
            case 'submit':
                return $this->surround('<input  type="submit" '.$id.' '.$class.' '.$value.'">');
                break;
            
            case 'text':
                return $this->surround('<input type="text" '.$name.' '.$id.' '.$class.' '.$value.' '.$placeholder.' '.$pattern.' '.$autocomplete.' '.$autofocus.' '.$checked.' '.$disabled.' '.$readonly.' '.$required.'>');
                break;
            
            default:
                return "error";
                break;
        }
    }
    
    public function textarea(array $attributes = []) {
        
        $name = $this->assignAttribute('name', $attributes);
        $autofocus = $this->assignAttribute('autofocus', $attributes); // "autofocus" or nothing
        $cols = $this->assignAttribute('cols', $attributes); // The visible width of the textarea (by default, will be overwritten by CSS)
        $disabled = $this->assignAttribute('disabled', $attributes); // "disabled" or nothing
        $form = $this->assignAttribute('form', $attributes); // Specifies one or more forms the textarea belongs to
        $maxlength = $this->assignAttribute('maxlength', $attributes); // The maximun number of characters allowed in the textarea
        $placeholder = $this->assignAttribute('placeholder', $attributes); // Text
        $readonly = $this->assignAttribute('readonly', $attributes); // "readonly" or nothing
        $required = $this->assignAttribute('required', $attributes); // "required" or nothing
        $rows = $this->assignAttribute('rows', $attributes); // The visible number of lines in the textarea (by default, will be overwritten by CSS)
        // Case where data are sent with the new Form
        if(isset($attributes['name'])){
            $value = trim($this->getValue($attributes['name']) ?? NULL); // Text
        } else {
            $value = NULL;
        }
        $id = $this->assignAttribute('id', $attributes);
        $class = $this->assignAttribute('class', $attributes);
        
        return $this->surround('<textarea '.$rows.' '.$cols.' '.$name.' '.$id.' '.$class.' '.$maxlength.' '.$form.' '.$placeholder.' '.$disabled.' '.$autofocus.' '.$readonly.' '.$required.'>'.$value.'</textarea>');
    }
    
    public function select(array $options = [], array $attributes = []) {
        $name = $this->assignAttribute('name', $attributes);
        $autofocus = $this->assignAttribute('autofocus', $attributes); // "autofocus" or nothing$cols = $this->assignAttribute('cols', $attributes); // The visible width of the textarea (by default, will be overwritten by CSS)
        $disabled = $this->assignAttribute('disabled', $attributes); // "disabled" or nothing
        $form = $this->assignAttribute('form', $attributes); // Specifies one or more forms the textarea belongs to
        $multiple = $this->assignAttribute('multiple', $attributes); // Specifies that multiple options can be selected a once
        $required = $this->assignAttribute('required', $attributes); // "required" or nothing
        $size = $this->assignAttribute('size', $attributes); // Number of visible option in the drop-down list ("0" by default)
        $id = $this->assignAttribute('id', $attributes);
        $class = $this->assignAttribute('class', $attributes);
        $Options = $this->getOptions($options);
        return $this->surround('<select '.$name.' '.$autofocus.' '.$disabled.' '.$form.' '.$multiple.' '.$required.' '.$size.'>'.$Options.'</select>');
    }
    
    private function getOptions($options) {
        $string = "";
        foreach ($options as $option) {
            $string .= $this->option($option);
        }
        $Options = rtrim($string, '<br>');
        return $Options;
    }
    
    private function option(array $attributes){
        $disabled = $this->assignAttribute('disabled', $attributes);
        $label = $this->assignAttribute('label', $attributes);
        $selected = $this->assignAttribute('selected', $attributes);
        $value = $this->assignAttribute('value', $attributes);
        $Value = $attributes['value'];
        return '<option '.$disabled.' '.$label.' '.$value.' '.$selected.'>'.$Value.'</option> <br>';
    }
    
    public function label(string $label = "", array $labelattributes = []) {
        $for = $this->assignAttribute('for', $labelattributes); // id to refer
        $form = $this->assignAttribute('from', $labelattributes);
        $id = $this->assignAttribute('id', $labelattributes);
        $class = $this->assignAttribute('class', $labelattributes);
        return '<label '.$for.' '.$form.' '.$id.' '.$class.'">'.$label.'</label>';
        
//        if($for === NULL){
//            return ('<label class="'.$class.'">'.$elementToLabel." ".$label.'</label>');
//        } elseif ($form === NULL) {
//            return ($elementToLabel.'<label for="'.$for.'" class="'.$class.'">'.$label.'</label>');
//        } else {
//            return ($elementToLabel.'<label for="'.$for.'" form="'.$form.'" class="'.$class.'">'.$label.'</label>');
//        }
    }
    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------
    
}
