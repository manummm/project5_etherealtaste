 <!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <link rel="icon" href="Content/images/favicon.ico">
        <!-- CSS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dancing+Script|Sansita">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="Content/css/style.css" />
        <link rel="stylesheet" href="Content/css/loaderSpinner.css" />
        <link rel="stylesheet" href="Content/css/checkboxToggleSwitch.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css " />
        <!-- Specific element -->
        <title><?= $title ?></title>
    </head>
    <body>
        <?php if(isset($_SESSION['session']['show_accueil']) && $_SESSION['session']['show_accueil'] == 'yes'){ ?>
        <div id="accueil">
            <div></div> <!-- For setting the backgroung in CSS -->
            <p>Etherealtaste</p>
            <div class="arrow"> <!-- For setting the arrow down -->
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <?php } ?>
        <!-- #global -->
        <div id="global">
            <!-- Navigation bar -->
            <nav id="navigationMenu" class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
                <a class="navbar-brand" href="index.php"><img src="Content/images/logo.png" id="logo" alt="Ethereal Taste"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarToggler">
                    <ul class="navbar-nav mr-auto">
                        <li></li>
                    </ul>                    
                    <ul class="navbar-nav" id="loginState">
                        <?php if(isset($_SESSION['session']['username'])){ ?>
                        <li class="nav-item"><a class="btn btn btn-outline-primary mr-2" href="index.php?action=profile"><i class="fa fa-user"></i><span id="username"><?= filter_var($_SESSION['session']['username'], FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></span></a></li>
                        <?php } ?>
                        <li class="nav-item">                                
                            <!-- Shows username if logged otherwise shows the button to trigger the modalLogin -->
                            <?php if(isset($_SESSION['session']['username'])){?> <a id="logout" class="btn btn-outline-primary mr-2" href="index.php?action=logout"><i class="fas fa-power-off"></i>Se déconnecter</a> <?php } else {?> <button type="button" class="btn btn-outline-primary mr-2" data-toggle="modal" data-target="#modalLogin">Se connecter</button> <?php } ?>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navigation bar -->
            <header></header>
            <main id="content">
                <!-- Specific element -->
                <?= $content ?>
            </main>
            <footer id="footer">
                <p>© 2019 Etherealtaste | All rights reserved.</p>
            </footer>
        </div>
        <!-- End #global -->
        
        <!-- Modal Log In -->
        <div id="modalLogin" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div class="modal-header">				
                        <h2 class="modal-title">Connexion Membre</h2>
                        <button id="closeLogin" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="loginForm" action="index.php?action=login" method="post">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" name="username" placeholder="Identifiant" autocomplete="on" required="required">
                            </div>
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" class="form-control" name="password" placeholder="Mot de passe" autocomplete="off" required="required">					
                            </div>
                            <div class="form-group">
                                <label for="rememberMe"><input type="checkbox" name="rememberMe" id="rememberMe" value="TRUE"> Se souvenir de moi</label><br>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" id="submitLogin" value="Se connecter">
                            </div>
                        </form>				

                    </div>
                    <div class="modal-footer">
                        <p>Vous n'avez pas de compte ?</p>
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  data-toggle="modal" data-target="#modalSignin">S'inscrire</button>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Log In -->
        <!-- Modal Sign In -->
        <div id="modalSignin" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div class="modal-header">				
                        <h2 class="modal-title">Inscription</h2>
                        <button id="closeSignin" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="signinForm" action="index.php?action=signin" method="post">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="text" class="form-control" name="username" placeholder="Identifiant" autocomplete="on" required="required">
                            </div>
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" class="form-control" name="password" placeholder="Mot de passe" autocomplete="off" required="required">					
                            </div>
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" class="form-control" name="passwordConfirmation" placeholder="Confirmer mot de passe" autocomplete="off" required="required">					
                            </div>
                            <div class="form-group">
                                <i class="fas fa-at" aria-hidden="true"></i>
                                <input type="text" class="form-control" name="email" placeholder="Email" autocomplete="email" required="required">					
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="S'inscrire">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <p>Vous avez déjà un compte ?</p>
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal"  data-toggle="modal" data-target="#modalLogin">Se connecter</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal Sign In -->
        <!-- Message success (Sign in / Log in / Activation) -->
        <div id="messageSuccess" class="alert alert-success" style="display: none;"></div>
        <!-- Message fail (Sign in / Log in / Activation) -->
        <div id="messageFail" class="alert alert-danger" style="display: none;"></div>
        <!-- Loader spinner -->
        <div class="loaderOverlay">
            <div class="cssloader">
                <div class="cssload-inner cssload-one"></div>
                <div class="cssload-inner cssload-two"></div>
                <div class="cssload-inner cssload-three"></div>
            </div>
        </div>
        
        <!-- Latest compiled jQuery, Popper.js and Bootstrap scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="Content/js/login.js"></script>
        <script src="Content/js/logout.js"></script>
        <script src="Content/js/tables.js"></script>
        <script src="Content/js/recipeUpdate.js"></script>
        <script src="Content/js/recipeForm.js"></script>
        <script src="Content/js/recipeDelete.js"></script>
        <script src="Content/js/tooltipSetup.js"></script>
        <script src="Content/js/recipeTabs.js"></script>
        <script src="Content/js/accueil.js"></script>
    </body>
</html>

