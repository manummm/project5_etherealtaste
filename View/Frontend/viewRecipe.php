<?php $this->title = " Etherealtaste | " . filter_var($recipe->getName(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?>

<article class="articleRecipe">
    <header>
        <a href="<?= "index.php?action=getRecipe&id=" . filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>">
            <h1><?= filter_var($recipe->getName(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></h1>
        </a>
        <span>par <?= filter_var($recipe->getAuthor(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?> | <time><?= filter_var($recipe->getOptimizedPublicationDate(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></time></span>
    </header>
    <section class="recipePicture recipePictureDefault">
        <img src="Content/images/default.png" alt="Photo de la recette">
    </section>
    <aside>
        <span class="iconBoxDarker"><meter min="10" low="30" optimum="50" high="80" max="100" value="<?= filter_var($recipe->getDifficulty_level_numeric(), FILTER_SANITIZE_NUMBER_INT); ?>"></meter> <?= filter_var($recipe->getDifficulty_level(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></span>
        <span class="iconBoxDarker"><i class="far fa-clock"></i> <?= filter_var($recipe->getPreparation_time(), FILTER_SANITIZE_NUMBER_INT); ?> min</span>
        <span class="iconBoxDarker"><i class="far fa-eye"></i> <?= filter_var($recipe->getNb_views(), FILTER_SANITIZE_NUMBER_INT); ?></span>
        <?php if(isset($_SESSION['session']['username']) && isset($_SESSION['session']['liked']) && $_SESSION['session']['liked'] == FALSE){ ?>
            <span class="iconBoxDarker"><a href="<?= "index.php?action=like&recipeId=" . filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT) . "&userId=" . $_SESSION['session']['id']; ?>"><i class="far fa-heart"></i> <?= filter_var($recipe->getNb_likes(), FILTER_SANITIZE_NUMBER_INT); ?></a></span>
            <?php }else if(isset($_SESSION['session']['username']) && isset($_SESSION['session']['liked']) && $_SESSION['session']['liked'] == $recipe->getId()){ ?>
            <span class="iconBoxDarker"><i class="fas fa-heart"></i> <?= filter_var($recipe->getNb_likes(), FILTER_SANITIZE_NUMBER_INT); ?></span>
            <?php }else{ ?>
            <span class="iconBoxDarker"><i class="far fa-heart"></i> <?= filter_var($recipe->getNb_likes(), FILTER_SANITIZE_NUMBER_INT); ?></span>
        <?php } ?>
        <span class="iconBoxDarker"><i class="fas fa-utensils"></i> <?= filter_var($recipe->getType_of_meal(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></span>
        <span class="iconBoxDarker">Régime: <?= filter_var($recipe->getDiet(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></span>
    </aside>
    <div class="recipeControls">
        <span id="tabIngredients" class="recipeTabs activeTab">Ingredients</span>
        <span id="tabPreparation" class="recipeTabs">Préparation</span>
    </div>
    <section class="recipeIngredients">
        <h3>Ingredients</h3>
        <p><?= filter_var($recipe->getIngredients(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></p>
    </section>
    <section class="recipePreparation">
        <h3>Préparation</h3>
        <p><?= filter_var($recipe->getPreparation(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></p>
    </section>
</article>