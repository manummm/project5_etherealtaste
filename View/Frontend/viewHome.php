<?php $this->title = " Etherealtaste | Home "; ?>

<section class="" id="recipeCards">
    <h2>Dernières recettes</h2>
    <?php foreach ($recipes as $recipe): ?>
        <article class="articleInCardFormat">
            <header>
                <div>
                    <a href="<?= "index.php?action=getRecipe&id=" . filter_var($recipe->getId(), FILTER_SANITIZE_NUMBER_INT); ?>">
                        <h3><?= filter_var($recipe->getName(), FILTER_SANITIZE_FULL_SPECIAL_CHARS); ?></h3>
                    </a>
                </div>
            </header>
            <div class="recipePictureDefault">
                <img src="Content/images/default.png" alt="Photo de la recette">
            </div>
            <footer>
                <div>
                    <span class="iconBox"><i class="far fa-eye"></i> <?= filter_var($recipe->getNb_views(), FILTER_SANITIZE_NUMBER_INT); ?></span>
                    <span class="iconBox"><i class="far fa-heart"></i> <?= filter_var($recipe->getNb_likes(), FILTER_SANITIZE_NUMBER_INT); ?></span>
                </div>
            </footer>
        </article>
        <hr />
    <?php endforeach; ?>


    <nav>
        <ul class="paginationRecipe">
            <li><a href="index.php?action=getRecipesByPage&page=1" rel="first">First</a></li>
            <li><a href="index.php?action=getRecipesByPage&page=<?= filter_var($previousPage, FILTER_SANITIZE_NUMBER_INT); ?>" rel="prev">Previous</a></li>

            <?php for($i = 1; $i <= $numberOfpages; $i++){ ?>
                <?php if($i == $activePage){ ?>
                    <li class="activePage"><a href="index.php?action=getRecipesByPage&page=<?= filter_var($i, FILTER_SANITIZE_NUMBER_INT); ?>"><?= filter_var($i, FILTER_SANITIZE_NUMBER_INT); ?></a></li>
                <?php } else { ?>
                    <li><a href="index.php?action=getRecipesByPage&page=<?= filter_var($i, FILTER_SANITIZE_NUMBER_INT); ?>"><?= filter_var($i, FILTER_SANITIZE_NUMBER_INT); ?></a></li>
                <?php } ?>
            <?php } ?>

            <li><a href="index.php?action=getRecipesByPage&page=<?= filter_var($nextPage, FILTER_SANITIZE_NUMBER_INT); ?>" rel="next">Next</a></li>
            <li><a href="<?= "index.php?action=getRecipesByPage&page=" . filter_var($numberOfpages, FILTER_SANITIZE_NUMBER_INT); ?>" rel="last">Last</a></li>
        </ul>
    </nav>
</section>