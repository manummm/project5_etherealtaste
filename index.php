<?php
// Start the session
session_start();

// Load the Autolader (which will manage to load the required class when needed)
// require_once() statement is identical to require()
// except PHP will check if the file has already been
// included, and if so, not include (require) it again.
require_once 'Autoloader.php';
Autoloader::register();

// Unset the variable that will allows the accueil animation
unset($_SESSION['session']['show_accueil']);

// Check if a session exists, if yes ==> return an User instance populated with the data $_SESSION['session'], if no ==>
// Check if a username cookie & a session token cookie exists, if yes ==> query the DB with the username from the cookie,
// compare the hash of the session token with the one from the DB and if true, set the profile in the $_SESSION
if(isset($_SESSION['session'])){
    $user = new User($_SESSION['session']);
} elseif(isset($_COOKIE['username']) && isset($_COOKIE['token_session'])){
    $ctrlUser = new ControllerUser();
    $user = $ctrlUser->getUser($_COOKIE['username']);
    $token_session_hash = $user->getTokenSession();
    if($ctrlUser->verify($_COOKIE['token_session'], $token_session_hash)){
        $profile = ['id' => $user->getId(), 'username' => $user->getUsername(), 'email' => $user->getEmail(), 'inscription_date' => $user->getInscriptionDate(), 'token_session' => $user->getTokenSession(), 'accreditation' => $user->getIdAccreditationLevel()];
        foreach ($profile as $key => $value) {
            $_SESSION['session'][$key] = $value;
        }
    }
} else {
    // Set the accreditation level to visitor (visitor = 0; admin = 1; member = 2)
    $_SESSION['session']['accreditation'] = 0;
}

$router = new Router();
$router->routeRequest();