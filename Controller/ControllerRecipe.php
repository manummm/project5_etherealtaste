<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controllerRecipe
 *
 * @author Manu
 */
class ControllerRecipe {
    
    private $recipeManager;

    public function __construct() {
        $this->recipeManager = new RecipeManager();
    }

    // Add a new recipe
    public function createRecipe($data) {
        // Create a new object Recipe with the data sent
        $recipe = new Recipe($data);
        // Add it into the database & return a message to confirm        
        $data = [];
        $savedRecipe = $this->recipeManager->addRecipe($recipe);
        if($savedRecipe === TRUE){
            $data['state'] = 'add';
            $data['return'] = '<strong>Votre recette vient d\'être publiée !</strong>';
            $id = $recipe->getId();
            $name = $recipe->getName();
            $author = $recipe->getAuthor();
            $ingredients = $recipe->getIngredients();
            $preparation = $recipe->getPreparation();
            $diet = $recipe->getDiet();
            $difficulty = $recipe->getDifficulty_level();
            $type_of_meal = $recipe->getType_of_meal();
            $preparation_time = $recipe->getPreparation_time();
            $publication_date = $recipe->getPublication_date();
            $publication_date_optmized = $recipe->getOptimizedPublicationDate();
            $datanamerecipe = urlencode($name);
            $dataauthorrecipe = urlencode($author);
            $dataingredientsrecipe = urlencode($ingredients);
            $datapreparationrecipe = urlencode($preparation);
            $datadietrecipe = urlencode($diet);
            $datadifficultyrecipe = urlencode($difficulty);
            $datatypeofmealrecipe = urlencode($type_of_meal);
            $datapreparationtimerecipe = urlencode($preparation_time);
            $line = 'line' . $id;
            $data['newline'] = '<tr id="' . $line .'"><td><a href="index.php?action=getRecipe&id=' . $id . '">' . $name . '</a></td><td><p>' . $author . '</p></td><td data-order="'. $publication_date .'"><time>' . $publication_date_optmized . '</time></td><td><button style="margin-right: 0.25rem;" type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#modalRecipe" data-idrecipe="' . $id . '" data-namerecipe="' . $datanamerecipe . '" data-authorrecipe="' . $dataauthorrecipe . '" data-ingredientsrecipe="' . $dataingredientsrecipe . '" data-preparationrecipe="' . $datapreparationrecipe . '" data-dietrecipe="' . $datadietrecipe . '" data-difficultyrecipe="' . $datadifficultyrecipe . '" data-typeofmealrecipe="' . $datatypeofmealrecipe . '" data-preparationtimerecipe="' . $datapreparationtimerecipe . '"><i class="fas fa-edit tooltip-edit"></i></button><form id="deleteRecipe' . $id . '" class="deleteRecipe" action="index.php?action=deleteRecipe" method="post"><input type="hidden" name="id" value="' . $id . '" /><button type="submit" class="btn btn-outline-danger"><i class="fas fa-trash-alt tooltip-delete" data-original-title="" title=""></i></button></form></td></tr>';
        }else{
            $data['state'] = 'fail';
            $data['return'] = 'Une erreur est survenue, la publication de votre recette a échoué, veuillez réessayer ultérieurement !';
        }
        echo json_encode($data);
    }
    
    // Show the details of a recipe
    public function getRecipe($id) {
        // Check if there is an user connected (not a visitor, accreditation id differente from 0)
        if(isset($_SESSION['session']['accreditation']) && $_SESSION['session']['accreditation'] !== 0){
            $userId = $_SESSION['session']['id'];
            $recipeId = $id;
            // Check if the user has already viewed this recipe
            $hasBeenViewed = $this->recipeManager->isViewed($userId, $recipeId);
            if($hasBeenViewed === TRUE){
                // Increase the view count of the entry [user-recipe] in the link table
                $this->recipeManager->incrementUserViews($userId, $recipeId);
            } else if($hasBeenViewed === FALSE) {
                // Add an entry [user-recipe] to the link table
                $this->recipeManager->addLinkUserRecipe($userId, $recipeId);
                // Increase the total view count of the recipe (all members) 
                $this->recipeManager->incrementViews($recipeId);
            }
            $hasBeenLiked = $this->recipeManager->isLiked($userId, $recipeId);
            if($hasBeenLiked === TRUE){
                $_SESSION['session']['liked'] = $recipeId;
            } else {
                $_SESSION['session']['liked'] =  FALSE;
            }
        }
        
        // Get the specific recipe from the database
        $recipe = $this->recipeManager->getRecipe($id);
        // Show it in a view
        $view = new View("Recipe");
        $view->generate(array('recipe' => $recipe));
    }
    
    // Update the details of a recipe
    public function updateRecipe($data) {
        // Create a new object Recipe with the data sent
        $recipe = new Recipe($data);
        // Update it into the database
        $data = [];
        $savedRecipe = $this->recipeManager->updateRecipe($recipe);
        if($savedRecipe === TRUE){
            $data['state'] = 'edit';
            $data['return'] = '<strong>Votre recette vient d\'être mise à jour !</strong>';
            $id = $recipe->getId();
            $name = $recipe->getName();
            $author = $recipe->getAuthor();
            $publication_date = $recipe->getPublication_date();
            $datanamerecipe = urlencode($name);
            $dataauthorrecipe = urlencode($author);
            $line = 'line' . $id;
            $data['newline'] = '<tr id="' . $line .'"><td><a href="index.php?action=getRecipe&id=' . $id . '"><h2 class="titleRecipe">' . $name . '</h2></a></td><td><p>' . $author . '</p></td><td><time>' . $publication_date . '</time></td><td><button style="padding-right: 10px;" type="button" class="btn-simple" data-toggle="modal" data-target="#modalRecipe" data-idrecipe="' . $id . '" data-namerecipe="' . $datanamerecipe . '" data-authorrecipe="' . $dataauthorrecipe . '"><i class="fas fa-edit tooltip-edit"></i></button><form id="deleteRecipe' . $id . '" class="deleteRecipe" action="index.php?action=deleteRecipe" method="post"><input type="hidden" name="id" value="' . $id . '" /><button type="submit" class="btn-simple"><i class="fas fa-trash-alt tooltip-delete" data-original-title="" title=""></i></button></form></td></tr>';
        }else{
            $data['state'] = 'fail';
            $data['return'] = 'Une erreur est survenue, la mise à jour de votre rectte a échoué, veuillez réessayer ultérieurement !';
        }
        echo json_encode($data);
    }
    
    // Delete a recipe from the database
    public function deleteRecipe($id) {
        $data = [];
        // Delete the recipe with the matching id from the database
        $deletedRecipe = $this->recipeManager->deleteRecipe($id);
        if($deletedRecipe === TRUE){
            $data['state'] = 'delete';
            $data['linetoedit'] = 'line' . $id;
            $data['return'] = '<strong>La recette vient d\'être supprimée !</strong>';
        }else{
            $data['state'] = 'fail';
            $data['return'] = 'Une erreur est survenue, la suppression de la recette à échouée, veuillez réessayer ultérieurement !';
        }
        echo json_encode($data);
    }
    
    // ((To do later ==> in AJAX))
    public function markAsLiked($userId, $recipeId) {
        $hasBeenLiked = $this->recipeManager->isLiked($userId, $recipeId);
        if($hasBeenLiked === TRUE){
            $this->getRecipe($recipeId);
        } else {
            $this->recipeManager->userLikes($userId, $recipeId);
            $this->recipeManager->incrementLikes($recipeId);
            $this->getRecipe($recipeId);
        }
    }
    
}
