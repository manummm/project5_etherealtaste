<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controllerHome
 *
 * @author Manu
 */
class ControllerHome {
    
    private $recipeManager;

    public function __construct() {
        $this->recipeManager = new RecipeManager();
    }

    // Show the homepage (with the list of the 5 last recipes published)
    public function home() {
        $this->paginationRecipe(1);
    }
    
    // Show the 5 recipes matching with the page number
    public function paginationRecipe($page) {
        $recipes = $this->recipeManager->getPaginatedRecipes($page);
        $numberOfRecipes = $this->recipeManager->getNumberOfRecipes();
        $numberOfRecipesWishedByPage = 5;
        $lastPageNumberOfRecipes = $numberOfRecipes % $numberOfRecipesWishedByPage;
        $numberOfpages = (($numberOfRecipes - $lastPageNumberOfRecipes) / $numberOfRecipesWishedByPage) + 1;
        $activePage = $page;
        $previousPage = $page - 1;
        $nextPage = $page + 1;
        $view = new View("Home");
        $view->generate(array('recipes' => $recipes, 'numberOfpages' => $numberOfpages, 'activePage' => $activePage, 'previousPage' => $previousPage, 'nextPage' => $nextPage));
    }
    
    // Show the admin panel
    public function profile() {
        $recipes = $this->recipeManager->getRecipes();
        $view = new View("Profile");
        $view->generate(array('recipes' => $recipes));
    } 
}
