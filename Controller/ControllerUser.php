<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllerUser
 *
 * @author Manu
 */
class ControllerUser {
    
    private $userManager;

    public function __construct() {
        $this->userManager = new UserManager();
    }
    
    // Log in
    public function login($username, $password) {
        
        // Declare the data array to return in json
        $data = [];
        
        // Get the user matching the given username from the database
        $user = $this->getUser($username);
        
        // Check if a user is found, if yes => proceed, otherwise store the failed attempt in $data[]
        if(isset($user)){
            
            // Get the password's hash of the user from the DB
            $passwordhash = $user->getPasswordHash();
            // Check if the hash of the given password is the same as the one from the DB, if yes => proceed, otherwise store the failed attempt in $data[]
            if($this->verify($password, $passwordhash)){
                
                // Create a session token (Used for the "remember me" option)
                $token_session = $this->token();
                // Hash the session token
                $token_session_hash = $this->hash($token_session);
                // Save the hash of the session token in the DB (under the username's data)
                $tokenSaved = $this->userManager->saveSessionToken($username, $token_session_hash);
                
                // Check if the token is saved, if yes => proceed, otherwise store the failed attempt in $data[]
                if($tokenSaved === TRUE){
                    
                    // Save the user profile in session variables
                    $_SESSION['session']['id'] = $user->getId();
                    $_SESSION['session']['username'] = ucfirst(strtolower($user->getUsername()));
                    $_SESSION['session']['email'] = $user->getEmail();
                    $_SESSION['session']['inscription_date'] = $user->getInscriptionDate();
                    $_SESSION['session']['token_session'] = $token_session;
                    $_SESSION['session']['accreditation'] = $user->getIdAccreditationLevel();
                    // If the "remember me" tick was checked, save the username & the session token in cookies
                    if(isset($_POST['rememberMe']) && $_POST['rememberMe'] == TRUE){
                        setcookie('username', $_SESSION['session']['username'], time() + 365*24*3600, '/', null, false, false);
                        setcookie('token_session', $_SESSION['session']['token_session'], time() + 365*24*3600, '/', null, false, false);
                    }
                    // Save the data to return in $data[]
                    $data['state'] = 'success';
                    $data['return'] = '<strong>Vous êtes maintenant connecté ! Bienvenue ' . $username . ' !</strong>';
                    
                } else {
                    $data['state'] = 'fail';
                    $data['return'] = 'Il y a eu un problème lors de votre identification, veuillez réessayer';
                }              
                
            } else {
                $data['state'] = 'fail';
                $data['return'] = 'Identifiant ou mot de passe incorrect';
            }
            
        } else {            
            $data['state'] = 'fail';
            $data['return'] = 'Identifiant ou mot de passe incorrect';           
        }
        echo json_encode($data);
    }
    
    // Log out
    public function logout() {
        $data = [];
        $username = $_SESSION['session']['username'];
        $loggedOut = $this->userManager->logout();
        if($loggedOut === TRUE){
            $data['state'] = 'success';
            $data['homepage'] = 'index.php?logout&username='.$username;
        }
        echo json_encode($data);
    }
    
    // Return an User instance if it matches the given username with a record in the DB, otherwise return NULL
    public function getUser($username) {
        return $this->userManager->getUser($username);
    }
    
    // Return a token of the given length
    private function token($length = 64) {
        $token = bin2hex(random_bytes($length));
        return $token;
    }
    
    // Return the result of the comparaison between the string hash with the given hash
    public function verify(string $string, $hash) {
        $check = (password_verify ($string , $hash));
        return $check;
    }
    
    // Return the hash of the given string
    private function hash(string $string) {        
        $hash = password_hash($string, PASSWORD_DEFAULT);
        return $hash;
    }
    
    // -------------------------------------------------------------------------
    // ------------ Not active yet, future features -----------------------------
    // -------------------------------------------------------------------------

//    // Sign in (Not active / Future Feature)
//    public function signin($username, $password, $passwordConfirmation, $email) {
//        $data = [];
//        // Checking if the email is correct
//        if(preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email)) {
//            // Check that pass & passconfirmation are the same
//            if($password == $passwordConfirmation) {
//                // Checking if the username is unique
//                $usermanager = new UserManager();
//                $checked = $usermanager->validUsername($username);
//                if($checked){
//                    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
//                    // Create a token
//                    $token_session = $this->token();
//                    // Save its hash in the database under the username profile
//                    $token_session_hash = $this->hash($token_session);
//                    $user = new User(array('username' => $username, 'passwordHash' => $passwordHash, 'email' => $email, 'tokenSession' => $token_session_hash));
//                    $registration = $this->userManager->signin($user);
//                    if($registration == TRUE){
//                        $this->sendConfirmationEmail($user->getUsername(), $user->getEmail(), $token_session);
//                        $data['state'] = 'success';
//                        $data['return'] = '<strong>Vous êtes maintenant inscrit, un email qui vient de vous être envoyé pour activer votre compte.</strong>';
//                    }else{
//                        // to be written
//                    }
//                } else {
//                    $error = [];
//                    $error['type'] = "username";
//                    $data['errorUsername'] = '<p class="error" style="color:red; text-align:center; padding:10px; margin-bottom: -1rem;">Cet identifiant est déjà utilisé.</p>';
//                    return $error ;
//                }
//            } else {
//                $error = [];
//                $error['type'] = 'password';
//                $data['errorPassword'] = '<p class="error" style="color:red; text-align:center; padding:10px; margin-bottom: -1rem;">Vos mots de passe ne sont pas identique.</p>';
//                return $error;
//            }
//        } else {
//            $error = [];
//            $error['type'] = "email";
//            $data['errorEmail'] = '<p class="error" style="color:red; text-align:center; padding:10px; margin-bottom: -1rem;">Votre adresse e-mail n\'est pas valide.</p>';
//            return $error;
//        }
//        echo json_encode($data);
//    }
//    
//    // Send an email with a link for activation of the user (Not active / Future Feature)
//    public function sendConfirmationEmail($username, $email, $token_session) {
//        // Receiver
//        $to = $email;
//        // Subject
//        $subject = 'Etherealtaste - Confirmation de votre email';
//        // Activation link
//        $activationLink = 'https://projet5.emmanuelserve.com/index.php?action=emailactivation&username='.urlencode($username).'&key='.urlencode($token_session);
//        // Message
//        $message =
//            '<td background=" XXX " bgcolor="rgba(51, 53, 52, 0.7)" width="500" height="500" valign="top">
//                <!--[if gte mso 9]>
//                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:500px;height:500px;">
//                    <v:fill type="tile" src=" XXX " color="whitesmoke" />
//                    <v:textbox inset="0,0,0,0">
//                <![endif]-->
//                <div>
//                    <div style="display:block; width:400px; height:400px; margin:50px auto; padding:20px; background: rgba(51, 53, 52, 0.7); color:whitesmoke;"><h1 style="text-align:center;">Etherealtaste</h1><br><br>     
//                    <p>Merci de votre inscription à Etherealtaste. Une dernière étape pour vous compter parmi nous ! Afin d\'activer votre compte, veuillez cliquer sur le lien ci dessous ou copier/coller dans votre navigateur internet.</p><br><br>
//                    <a style="display: block; color: #fff; background-color: #007bff; border-color: #007bff; min-height: 40px; border-radius: 3px; margin:auto; padding: .5rem 1rem; font-size: 1.25rem; text-align: center; vertical-align: middle; line-height: 1.5; max-width: 350px;" href="'.$activationLink.'">Lien d\'activation</a><br><br>
//                    <p style="text-align:center;">--------------------------------------------------------------------------------------<br>
//                    Ceci est un mail automatique, Merci de ne pas y répondre.<br></div></p>
//                </div>
//                <!--[if gte mso 9]>
//                    </v:textbox>
//                    </v:rect>
//                <![endif]-->
//            </td>';
//        // Mail HTML => header Content-type must be defined
//        $headers[] = 'MIME-Version: 1.0';
//        $headers[] = 'Content-type: text/html; charset=UTF-8';
//        // Additional headers
//        $headers[] = 'From: Etherealtaste@gmail.com';
//        // Send
//        mail($to, $subject, $message, implode("\r\n", $headers));
//    }
//    
//    // Check if the key is valid and if so call the activation method of the UserManager
//    public function emailActivation($username, $token_session){
//        // (Not active / Future Feature)
//    }
//    
//    // Delete a user
//    public function delete($id) {
//        // (Not active / Future Feature)
//    }    
//    
//    // Give or remove permission(s) to the specified User
//    public function modifyAccreditationLevel($id) {
//        // (Not active / Future Feature)
//    }
//    
//    // Allow to modify an Username
//    public function editUsername($id) {
//        // (Not active / Future Feature)
//    }
    
}