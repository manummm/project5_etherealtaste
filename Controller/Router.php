<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of router
 *
 * @author Manu
 */
class Router {
    
    
    private $ctrlHome;
    private $ctrlRecipe;
    private $ctrlUser;
    private $action;

    public function __construct() {
        $this->ctrlHome = new ControllerHome();
        $this->ctrlRecipe = new ControllerRecipe();
        $this->ctrlUser = new ControllerUser();
        $this->action = $this->getParameter($_GET, 'action');
    }

    // Process an incoming request
    public function routeRequest() {
        try {
            switch ($this->action) {
                // CREATE (recipe)
                case "addRecipe":
                    // Check for the access rights ----------------------------------------------------------------------------------
                    $neededAccreditation = ['Admin'];
                    if(($this->hasAccessRights($neededAccreditation)) == 'Denied'){
                        throw new Exception("Action invalide ou vous n'avez pas les permissions  nécessaires accéder à cette page");
                        break;                         
                    }
                    // --------------------------------------------------------------------------------------------------------------
                    
                    $name = $this->getParameter($_POST, 'name');
                    $author = $this->getParameter($_POST, 'author');
                    $ingredients = $this->getParameter($_POST, 'ingredients');
                    $preparation = $this->getParameter($_POST, 'preparation');
                    $diet = $this->getParameter($_POST, 'diet');
                    $difficulty_level = $this->getParameter($_POST, 'difficulty_level');
                    $type_of_meal = $this->getParameter($_POST, 'type_of_meal');
                    $preparation_time = $this->getParameter($_POST, 'preparation_time');
                    $publication_date = date("Y-m-d H:i:s");
                    $this->ctrlRecipe->createRecipe(array('name' => $name, 'author' => $author, 'ingredients' => $ingredients, 'preparation' => $preparation, 'diet' => $diet, 'difficulty_level' => $difficulty_level, 'type_of_meal' => $type_of_meal, 'preparation_time' => $preparation_time, 'publication_date' => $publication_date));
                    break; // End the switch ($this->action)

                // READ (recipe)
                case "getRecipe":                
                    $id = (int)($this->getParameter($_GET, 'id'));
                    if ($id != 0) {
                        $this->ctrlRecipe->getRecipe($id);
                    }
                    else {
                        throw new Exception("Invalid recipe ID");
                    }
                    break; // End the switch ($this->action)

                // UPDATE (recipe)
                case "updateRecipe":
                    // Check for the access rights ----------------------------------------------------------------------------------
                    $neededAccreditation = ['Admin'];
                    if(($this->hasAccessRights($neededAccreditation)) == 'Denied'){
                        throw new Exception("Action invalide ou vous n'avez pas les permissions  nécessaires accéder à cette page");
                        break;                         
                    }
                    // --------------------------------------------------------------------------------------------------------------
                    
                    $id = (int)($this->getParameter($_POST, 'id'));
                    $name = $this->getParameter($_POST, 'name');
                    $author = $this->getParameter($_POST, 'author');
                    $ingredients = $this->getParameter($_POST, 'ingredients');
                    $preparation = $this->getParameter($_POST, 'preparation');
                    $diet = $this->getParameter($_POST, 'diet');
                    $difficulty_level = $this->getParameter($_POST, 'difficulty_level');
                    $type_of_meal = $this->getParameter($_POST, 'type_of_meal');
                    $nb_views = (int)($this->getParameter($_POST, 'nb_views'));
                    $nb_likes = (int)($this->getParameter($_POST, 'nb_likes'));
                    $preparation_time = $this->getParameter($_POST, 'preparation_time');
                    $publication_date = $this->getParameter($_POST, 'publication_date');
                    
                    if ($id != 0) {
                        $this->ctrlRecipe->updateRecipe(
                            array('id' => $id,
                                'name' => $name,
                                'author' => $author,
                                'ingredients' => $ingredients,
                                'preparation' => $preparation,
                                'diet' => $diet,
                                'difficulty_level' => $difficulty_level,
                                'type_of_meal' => $type_of_meal,
                                'nb_views' => $nb_views,
                                'nb_likes' => $nb_likes,
                                'preparation_time' => $preparation_time,
                                'publication_date' => $publication_date
                            )
                        );
                    }
                    else {
                        throw new Exception("Invalid recipe ID");
                    }                    
                    break; // End the switch ($this->action)

                // DELETE (recipe)
                case "deleteRecipe":
                    // Check for the access rights ----------------------------------------------------------------------------------
                    $neededAccreditation = ['Admin'];
                    if(($this->hasAccessRights($neededAccreditation)) == 'Denied'){
                        throw new Exception("Action invalide ou vous n'avez pas les permissions  nécessaires accéder à cette page");
                        break;                         
                    }
                    // --------------------------------------------------------------------------------------------------------------
                    
                    $id = (int)($this->getParameter($_POST, 'id'));
                    if ($id != 0) {
                        $this->ctrlRecipe->deleteRecipe($id);
                    }
                    else {
                        throw new Exception("Invalid recipe ID");
                    }
                    break; // End the switch ($this->action)
                
                // LIKE (recipe)
                case "like":
                    // Check for the access rights ----------------------------------------------------------------------------------
                    $neededAccreditation = ['Member','Admin'];
                    if(($this->hasAccessRights($neededAccreditation)) == 'Denied'){
                        throw new Exception("Action invalide ou vous n'avez pas les permissions  nécessaires accéder à cette page");
                        break;                         
                    }
                    // --------------------------------------------------------------------------------------------------------------
                    
                    $userId = (int)($this->getParameter($_GET, 'userId'));
                    $recipeId = (int)($this->getParameter($_GET, 'recipeId'));
                    if ($userId != 0 && $recipeId != 0) {
                        $this->ctrlRecipe->markAsLiked($userId, $recipeId);
                    }
                    else {
                        throw new Exception("Invalid recipe ID");
                    }
                    break; // End the switch ($this->action)
                    
                // LOGIN
                case "login":
                    $username = $this->getParameter($_POST, 'username');
                    $password = $this->getParameter($_POST, 'password');
                    $this->ctrlUser->login($username, $password);
                    break;

                // SIGNIN
                case "signin":
                    $username = $this->getParameter($_POST, 'username');
                    $password = $this->getParameter($_POST, 'password');
                    $passwordConfirmation = $this->getParameter($_POST, 'passwordConfirmation');
                    $email = $this->getParameter($_POST, 'email');
                    $this->ctrlUser->signin($username, $password, $passwordConfirmation, $email);
                    break;
                
                // LOGOUT
                case "logout":
                    $this->ctrlUser->logout();
                    break;
                
                // SHOW THE ADMIN PANEL
                case "profile":
                    // Check for the access rights ----------------------------------------------------------------------------------
                    $neededAccreditation = ['Member','Admin'];
                    if(($this->hasAccessRights($neededAccreditation)) == 'Denied'){
                        throw new Exception("Action invalide ou vous n'avez pas les permissions  nécessaires accéder à cette page");
                        break;                         
                    }
                    // --------------------------------------------------------------------------------------------------------------
                    
                    $this->ctrlHome->profile();
                    break;
                    
                //
                case "getRecipesByPage":
                    $page = (int)($this->getParameter($_GET, 'page'));
                    $this->ctrlHome->paginationRecipe($page);
                    break;
                        
                // REDIRECT TO THE HOME PAGE
                case NULL:
                    $_SESSION['session']['show_accueil'] = 'yes';
                    // Create a cookie 'skip_accueil' with the value of $_COOKIE if it exists, otherwise it's set to 1
                    setcookie('skip_accueil', isset($_COOKIE['skip_accueil']) ? ++$_COOKIE['skip_accueil'] : 1);
                    $this->ctrlHome->home();
                    break;

                default:
                    throw new Exception("Action invalide ou vous n'avez pas les permissions  nécessaires accéder à cette page");
                    break;                        
            }
        }
        catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }
  
    // Return the named parameter in the array or return NULL if it doesn't exist
    private function getParameter($array, $name) {
        if (isset($array[$name])) {
            return $array[$name];
        }
        else
            return NULL;
    }

    // Show an error
    private function error($msgError) {
        $view = new View("Error");
        $view->generate(array('msgError' => $msgError));
    }
    
    private function hasAccessRights(array $accessRights = ['Visitor']) {
        $accreditationId = $_SESSION['session']['accreditation'] ?? 0;
        switch ($accreditationId) {
            case 0:
                $accreditation = 'Visitor';
                break;
            
            case 1:
                $accreditation = 'Admin';
                break;
            
            default:
                $accreditation = 'Visitor';
                break;
        }
        if(in_array($accreditation, $accessRights)){
            $result = 'Allowed';
        } else {
            $result = 'Denied';
        }
        return $result;
    }

}
